/* genrc - generic system initialization script helper
   Copyright (C) 2018-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <syslog.h>
#include <time.h>
#include <assert.h>

static void
xpipe(int p[2])
{
	if (pipe(p)) {
		system_error(errno, "pipe");
		exit(1);
	}
}

static inline int
max(int a, int b)
{
	return a > b ? a : b;
}

static void
write_pid_file(pid_t pid)
{
	if (genrc_create_pidfile) {
		FILE *fp;
		fp = fopen(genrc_create_pidfile, "w");
		fprintf(fp, "%lu\n", (unsigned long)pid);
		fclose(fp);
	}
}

static void
unlink_pid_file(void)
{
	if (genrc_create_pidfile)
		unlink(genrc_create_pidfile);
}

#define LOGBUFSIZE 1024

enum log_type {
	GENRC_LOG_SYSLOG,  /* Log to syslog. */
	GENRC_LOG_FILE,    /* Log to a file. */
	GENRC_LOG_REF      /* Log to a file referred to by another buffer. */
};

struct log_buffer {
	char buf[LOGBUFSIZE];
	size_t pos;
	enum log_type type;
	union {
		int prio;
		struct {
			int fd;
			time_t ts;
			char *name;
			struct log_buffer *referrer;
		} file;
		struct {
			int lock;
			time_t ts;
			struct log_buffer *refbuf;
		} ref;
	};
};

static void
log_buffer_init(struct log_buffer *lb, int type)
{
	lb->buf[sizeof(lb->buf) - 1] = 0;
	lb->pos = 0;
	lb->type = type;
}

static void
log_buffer_init_syslog(struct log_buffer *lb, int prio)
{
	log_buffer_init(lb, GENRC_LOG_SYSLOG);
	lb->prio = prio;
}

static void
log_buffer_reopen(struct log_buffer *lb)
{
	if (lb->type == GENRC_LOG_FILE) {
		if (lb->file.fd != -1)
			close(lb->file.fd);
		lb->file.fd = open(lb->file.name, O_CREAT|O_WRONLY|O_APPEND, 0644);
		if (lb->file.fd == -1) {
			genrc_error("can't open file %s for append: %s",
				    lb->file.name, strerror(errno));
		}
	}
}

static void
log_buffer_init_file(struct log_buffer *lb, char const *filename)
{
	log_buffer_init(lb, GENRC_LOG_FILE);
	lb->file.name = xstrdup(filename);
	lb->file.fd = -1;
	lb->file.referrer = NULL;
	log_buffer_reopen(lb);
}

static void
log_buffer_init_ref(struct log_buffer *lb, struct log_buffer *ref)
{
	assert(ref->type == GENRC_LOG_FILE);
	log_buffer_init(lb, GENRC_LOG_REF);
	lb->ref.lock = 0;
	lb->ref.refbuf = ref;
	ref->file.referrer = lb;
}

static int
full_write(int fd, char const *buf, size_t size)
{
	while (size > 0) {
		size_t n = write(fd, buf, size);
		if (n == -1)
			return -1;
		if (n == 0) {
			errno = ENOSPC;
			return -1;
		}
		buf += n;
		size -= n;
	}
	return 0;
}

static void log_ref_flush(struct log_buffer *lb);
static int log_buffer_copy(struct log_buffer *lb, time_t ts,
			   char const *buf, size_t len);

static void
log_buffer_flush(struct log_buffer *lb)
{
	lb->buf[lb->pos] = 0;
	switch (lb->type) {
	case GENRC_LOG_SYSLOG:
		syslog(lb->prio, "%s", lb->buf);
		break;
	case GENRC_LOG_FILE:
		if (lb->file.fd != -1) {
			char *tbuf = ctime(&lb->file.ts);
			char *p = strchr(tbuf, '\n');
			if (full_write(lb->file.fd, tbuf, p - tbuf) ||
			    full_write(lb->file.fd, ": ", 2) ||
			    full_write(lb->file.fd, lb->buf, lb->pos) ||
			    full_write(lb->file.fd, "\n", 1))
				syslog(LOG_CRIT, "error writing to %s: %m",
				       lb->file.name);
		}
		break;
	case GENRC_LOG_REF:
		if (log_buffer_copy(lb->ref.refbuf, lb->ref.ts,
				    lb->buf, lb->pos)) {
			lb->ref.lock = 1;
			return;
		}
	}
	lb->pos = 0;
	if (lb->type == GENRC_LOG_FILE && lb->file.referrer)
		log_ref_flush(lb->file.referrer);
}

static void
log_ref_flush(struct log_buffer *lb)
{
	assert(lb->type == GENRC_LOG_REF);
	if (lb->ref.lock) {
		lb->ref.lock = 0;
		log_buffer_flush(lb);
	}
}

static int
log_buffer_copy(struct log_buffer *lb, time_t ts, char const *buf, size_t len)
{
	assert(lb->type == GENRC_LOG_FILE);
	if (lb->pos > 0)
		return -1;
	lb->file.ts = ts;
	memcpy(lb->buf, buf, len);
	lb->pos += len;
	log_buffer_flush(lb);
	return 0;
}

static int
log_buffer_read(int fd, struct log_buffer *lb)
{
	int rc;

	switch (lb->type) {
	case GENRC_LOG_FILE:
		if (lb->pos == 0)
			lb->file.ts = time(NULL);
		break;

	case GENRC_LOG_REF:
		if (lb->ref.lock)
			return 0;
		if (lb->pos == 0)
			lb->ref.ts = time(NULL);

	default:
		break;
	}

	rc = read(fd, lb->buf + lb->pos, 1);
	if (rc == -1) {
		syslog(LOG_ERR, "read: %m");
		return -1;
	} else if (rc == 1) {
		if (lb->pos == sizeof(lb->buf)-1 || lb->buf[lb->pos] == '\n')
			log_buffer_flush(lb);
		else
			lb->pos++;
	} else
		return -1;
	return 0;
}

struct restart_cond {
	struct restart_cond *next;
	int type;
	int negate;
	int numc;
	int numv[1];
};

struct restart_cond *restart_head, *restart_tail;

static int
restart_on(int type, int num)
{
	struct restart_cond *cond;

	for (cond = restart_head; cond; cond = cond->next) {
		if (cond->type == type) {
			int result = cond->negate;
			int i;
			for (i = 0; i < cond->numc; i++) {
				if (cond->numv[i] == num) {
					result = !result;
					break;
				}
			}
			if (result)
				return 1;
		}
	}
	return 0;
}

typedef int (*RESTART_STON)(char const *);

static RESTART_STON restart_ston[] = { str_to_int, str_to_sig };
static char const *restart_what[] = { "exit status", "signal" };

void
add_restart_condition(int type, char const *arg)
{
	struct wordsplit ws;
	size_t i;
	int negate = 0;
	struct restart_cond *cond;
	RESTART_STON ston = restart_ston[type];

	if (arg[0] == '!') {
		negate = 1;
		arg++;
	}

	ws.ws_delim = ",";
	ws.ws_error = genrc_error;
	if (wordsplit(arg, &ws,
		      WRDSF_NOCMD
		      | WRDSF_NOVAR
		      | WRDSF_DELIM
		      | WRDSF_ENOMEMABRT
		      | WRDSF_SHOWERR
		      | WRDSF_ERROR))
		exit(1);

	if (ws.ws_wordc == 0)
		usage_error("empty restart condition");

	cond = xmalloc(sizeof(*cond)
		       + (ws.ws_wordc - 1) * sizeof(cond->numv[0]));
	cond->next = NULL;
	cond->type = type;
	cond->negate = negate;
	cond->numc = ws.ws_wordc;
	for (i = 0; i < ws.ws_wordc; i++) {
		int n = ston(ws.ws_wordv[i]);
		if (n == -1)
			usage_error("bad %s: %s", restart_what[type],
				    ws.ws_wordv[i]);
		cond->numv[i] = n;
	}

	if (restart_tail)
		restart_tail->next = cond;
	else
		restart_head = cond;
	restart_tail = cond;
}

/* SIGHUP detection */
static int volatile hup_received;

static void
sighup(int sig)
{
	hup_received++;
}

static pid_t
start_logger(int out, int err)
{
	fd_set rdset;
	int nfd = max(out, err) + 1;
	struct log_buffer obuf, ebuf;

	pid_t pid = fork();
	if (pid != 0)
		return pid;

	mf_proctitle_format("logger [%s]", genrc_command);
	hup_received = 0;

	/* Actual logger starts here: */
	genrc_openlog();
	if (genrc_log_file) {
		log_buffer_init_file(&obuf, genrc_log_file);
		log_buffer_init_ref(&ebuf, &obuf);
	} else {
		if (genrc_log_out_file)
			log_buffer_init_file(&obuf, genrc_log_out_file);
		else
			log_buffer_init_syslog(&obuf, LOG_INFO);
		if (genrc_log_err_file)
			log_buffer_init_file(&ebuf, genrc_log_err_file);
		else
			log_buffer_init_syslog(&ebuf, LOG_ERR);

		if (obuf.type == GENRC_LOG_FILE &&
		    ebuf.type == GENRC_LOG_FILE) {
			struct stat ost, est;

			if (fstat(obuf.file.fd, &ost) == 0) {
				if (fstat(ebuf.file.fd, &est) == 0) {
					if (ost.st_dev == est.st_dev &&
					    ost.st_ino == est.st_ino) {
						close(ebuf.file.fd);
						log_buffer_init_ref(&ebuf,
								    &obuf);
					}
				} else
					system_error(errno, "fstat(%s)",
						     ebuf.file.name);
			} else
				system_error(errno, "fstat(%s)",
					     obuf.file.name);
		}
	}
	while (1) {
		int rc;

		if (hup_received) {
			log_buffer_reopen(&obuf);
			log_buffer_reopen(&ebuf);
			hup_received = 0;
		}

		FD_ZERO(&rdset);
		nfd = 0;
		if (out != -1) {
			FD_SET(out, &rdset);
			nfd = out;
		}
		if (err != -1) {
			FD_SET(err, &rdset);
			if (err > out)
				nfd = err;
		}
		if (nfd == 0)
			break;

		rc = select(nfd + 1, &rdset, NULL, NULL, NULL);
		if (rc == -1) {
			if (errno == EINTR)
				continue;
			syslog(LOG_CRIT, "logger: select: %m");
			exit(1);
		}

		if (FD_ISSET(out, &rdset)) {
			if (log_buffer_read(out, &obuf)) {
				close(out);
				out = -1;
			}
		}
		if (FD_ISSET(err, &rdset)) {
			if (log_buffer_read(err, &ebuf)) {
				close(err);
				err = -1;
			}
		}
	}
	exit(0);
}

/* Indices to the array of PIDs */
enum {
	PID_LOGGER,  /* Slot for logger process PID. */
	PID_CHILD    /* Slot for child process PID. */
};

/*
 * Wait for one of the subprocesses to terminate.
 * If child terminates, check if its termination condition (status code
 * or signal number) matches declared restart conditions.
 * If logger terminates with abnormal status, terminate the child and
 * restart it unconditionally.
 *
 * The function returns if the child needs to be restarted.  Otherwise,
 * it exits.
 */
void
wait_loop(pid_t cpid[])
{
	int restart = 0;

	while (1) {
		int status;
		pid_t pid;

		pid = waitpid((pid_t)-1, &status, 0);

		if (pid == -1) {
			if (errno == EINTR) {
				if (hup_received) {
					if (cpid[PID_LOGGER] != -1)
						kill(cpid[PID_LOGGER], SIGHUP);
				}
				continue;
			}
			genrc_error("waitpid: %s", strerror(errno));
			abort();
		}

		if (pid == cpid[PID_CHILD]) {
			write_pid_file(getpid());
			if (WIFEXITED(status)) {
				int code = WEXITSTATUS(status);
				syslog(LOG_INFO, "%s exited with status %d",
				       genrc_program, code);
				if (!restart)
					restart = restart_on(RESTART_ON_EXIT, code);
			} else if (WIFSIGNALED(status)) {
				char const *coremsg = "";
				int sig = WTERMSIG(status);
#ifdef WCOREDUMP
				if (WCOREDUMP(status))
					coremsg = " (core dumped)";
#endif
				syslog(LOG_INFO, "%s terminated on signal %d%s",
				       genrc_program, sig, coremsg);
				if (!restart)
					restart = restart_on(RESTART_ON_SIGNAL, sig);
			} else if (WIFSTOPPED(status)) {
				syslog(LOG_INFO, "%s stopped on signal %d",
				       genrc_program, WSTOPSIG(status));
			} else {
				syslog(LOG_INFO, "%s terminated; status %d",
				       genrc_program, status);
			}
			if (cpid[PID_LOGGER] != -1)
				kill(cpid[PID_LOGGER], SIGKILL);
			if (restart)
				return;
			break;
		}

		if (pid == cpid[PID_LOGGER]) {
			syslog(LOG_CRIT, "logger exited");
			cpid[PID_LOGGER] = -1;
			if (WIFEXITED(status)) {
				if (WEXITSTATUS(status) == 0) {
					/* Wait for the child to exit. */
					continue;
				}
			}
			// FIXME
			syslog(LOG_CRIT, "stopping %lu and restarting",
			       (unsigned long) cpid[PID_CHILD]);
			kill(cpid[PID_CHILD], SIGTERM);
			restart = 1;
			continue;
		}
	}
	unlink_pid_file();
	_exit(0);
}

pid_t
start_command(int out, int err)
{
	pid_t pid;

	pid = fork();

	setsid();
	if (pid == -1) {
		system_error(errno, "pipe");
		return -1;
	}

	if (pid == 0) {
		int p[2];

		p[0] = out;
		p[1] = err;
		spawn(p);
	}

	write_pid_file(pid);

	return pid;
}

/* Restart rate control */

/* Consider the number of restarts during this interval */
#define TESTTIME 2*60
/* Stop respawning and go to sleep if it exceeds this number */
#define MAXSPAWN 10
/* Sleep that much seconds, then retry */
#define SLEEPTIME 5*60

struct ratectl {
	time_t start_time;   /* Start of the test interval */
	unsigned failcount;  /* Number of restarts done so far */
};

static void
check_failure_rate(struct ratectl *rate)
{
	time_t now;
	struct timeval start, stop, ttw;

	time(&now);
	if (rate->start_time + TESTTIME > now)
		rate->failcount++;
	else {
		rate->failcount = 0;
		rate->start_time = now;
	}

	if (rate->failcount > MAXSPAWN) {
		syslog(LOG_NOTICE,
		       "%s respawning too fast; disabled for %d minutes",
		       genrc_program, SLEEPTIME / 60);

		gettimeofday(&stop, NULL);
		stop.tv_sec += SLEEPTIME;
		while (1) {
			gettimeofday(&start, NULL);
			if (timercmp(&start, &stop, >=))
				break;
			timersub(&stop, &start, &ttw);
			if (select(0, NULL, NULL, NULL, &ttw) < 0) {
				if (errno == EINTR) {
					if (hup_received) {
						hup_received = 0;
						break;
					}
				} else {
					system_error(errno, "select");
					break;
				}
			}
		}

		rate->failcount = 0;
	}
}

int
sentinel(void)
{
	pid_t pid;
	struct ratectl ctl;
	struct sigaction act;

	/* Detach from the controlling terminal */
	pid = fork();
	if (pid == -1) {
		system_error(errno, "fork");
		return -1;
	}
	if (pid) /* master */
		return 0;
	/* Run as a session leader */
	setsid();

	pid = fork();
	if (pid == -1) {
		system_error(errno, "fork");
		exit(1);
	}
	if (pid)
		_exit(0);

	/* Grand-child */
	close_fds_from(0);
	if (open("/dev/null", O_RDONLY) == -1) {
		system_error(errno, "open");
		_exit(1);
	}
	if (open("/dev/null", O_WRONLY) == -1) {
		system_error(errno, "open");
		_exit(1);
	}
	if (dup(1) == -1) {
		system_error(errno, "dup");
		_exit(1);
	}

	mf_proctitle_format ("sentinel [%s]", genrc_command);
	act.sa_handler = sighup;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaction(SIGHUP, &act, NULL);

	ctl.start_time = 0;
	ctl.failcount = 0;
	while (1) {
		int errpipe[2], outpipe[2];
		pid_t cpid[2];

		xpipe(errpipe);
		xpipe(outpipe);

		cpid[PID_LOGGER] = start_logger(outpipe[0], errpipe[0]);
		if (cpid[PID_LOGGER] == -1)
			_exit(127);

		cpid[PID_CHILD] = start_command(outpipe[1], errpipe[1]);
		if (cpid[PID_CHILD] == -1)
			_exit(127);

		close(outpipe[0]);
		close(outpipe[1]);
		close(errpipe[0]);
		close(errpipe[1]);

		wait_loop(cpid);
		check_failure_rate(&ctl);
		syslog(LOG_INFO, "restarting %s", genrc_program);
	}
	_exit(1);
}
