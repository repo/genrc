/* genrc - generic system initialization script helper
   Copyright (C) 2018-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"

struct genrc_pid_source {
	char *name;
	size_t maxwords;
	GENRC_PID_CLOSURE *(*init)(int argc, char **argv);
};

struct genrc_pid_source sourcetab[] = {
	{ "FILE",   2, genrc_pid_file_init },
	{ "CONFIG", 4, genrc_pid_config_init },
	{ "GREP",   3, genrc_pid_grep_init },
	{ "PROC",   3, genrc_pid_proc_init },
	{ "PS",     3, genrc_pid_ps_init },
	{ NULL }
};

GENRC_PID_CLOSURE *
get_pid_closure(char const *str)
{
	struct genrc_pid_source *s;
	struct wordsplit ws;
	GENRC_PID_CLOSURE *pc;
	size_t len = strcspn(str, ":");
	int wsflags;

	if (len == 0)
		usage_error(ENV_GENRC_PID_FROM " argument is empty");
	for (s = sourcetab; ; s++) {
		if (!s->name)
			usage_error("%s: unsupported PID source", str);
		if (strlen(s->name) == len && memcmp(s->name, str, len) == 0)
			break;
	}

	ws.ws_error = genrc_error;
	ws.ws_delim = ":";
	ws.ws_escape[WRDSX_WORD] = ws.ws_escape[WRDSX_QUOTE] = "\\\\::";
	wsflags =  WRDSF_NOCMD
		   | WRDSF_NOVAR
		   | WRDSF_QUOTE
		   | WRDSF_ESCAPE
		   | WRDSF_DELIM
		   | WRDSF_ENOMEMABRT
		   | WRDSF_SHOWERR
		   | WRDSF_ERROR;
	if (s->maxwords) {
		ws.ws_maxwords = s->maxwords;
		ws.ws_options = WRDSO_MAXWORDS;
		wsflags |= WRDSF_OPTIONS;
	}
	if (wordsplit(str, &ws, wsflags))
		exit(1);
	pc = s->init(ws.ws_wordc, ws.ws_wordv);
	if (!pc)
		exit(1);
	pc->name = xstrdup(str);
	return pc;
}

GENRC_PID_CLOSURE *
get_pid_closure_file(char const *file)
{
	GENRC_PID_CLOSURE *pc;
	char *args[3] = {
		"FILE",
		(char*) file,
		NULL
	};
	pc = genrc_pid_file_init(2, args);
	if (!pc)
		exit(1);
	pc->name = xstrdup(args[0]);
	return pc;
}

int
get_pid_list(GENRC_PID_CLOSURE *clos, PIDLIST *plist)
{
	pidlist_clear(plist);
	return clos->pid(clos, plist);
}
