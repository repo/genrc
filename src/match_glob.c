/* genrc - generic system initialization script helper
   Copyright (C) 2018-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"
#define _GNU_SOURCE 1
#include <fnmatch.h>

void
match_glob_init(PROCSCANBUF buf, char const *pattern)
{
	buf->pattern = xstrdup(pattern ? pattern : genrc_program);
}

void
match_glob_free(PROCSCANBUF buf)
{
	free(buf->pattern);
}

int
match_glob(PROCSCANBUF buf, char const *arg)
{
	return fnmatch((char*)buf->pattern, arg,
		       (buf->flags & PROCF_ICASE) ? FNM_CASEFOLD : 0);
}
