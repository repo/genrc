/* genrc - generic system initialization script helper
   Copyright (C) 2018-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"

int
com_reload(void)
{
	PIDLIST pids;

	if (genrc_no_reload)
		return com_restart();
	pidlist_init(&pids);
	if (get_pid_list(genrc_pid_closure, &pids))
		return 1;
	if (genrc_verbose > 1) {
		printf("Reloading %s: sending %s to %s ",
		       genrc_program, strsignal(genrc_signal_reload),
		       pids.pidc > 1 ? "PIDs" : "PID");
		printf("%lu", (unsigned long)pids.pidv[0]);
		if (pids.pidc > 1) {
			int i;
			for (i = 0; i < pids.pidc; i++ )
				printf(",%lu", (unsigned long)pids.pidv[i]);
		}
		putchar('\n');
	}
	pidlist_kill(&pids, genrc_signal_reload);
	return 0;
}
