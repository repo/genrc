/* genrc - generic system initialization script helper
   Copyright (C) 2023-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#define OPTFLAG_DEFAULT      OPTFLAG_OPTION
#define OPTFLAG_OPTION       0x0000
#define OPTFLAG_ALIAS        0x0001
#define OPTFLAG_DOC          0x0002
#define OPTFLAG_HIDDEN       0x0004
#define OPTFLAG_ARG_OPTIONAL 0x0008
#define OPTFLAG_LIST         0x0010
#define OPTFLAG_SUBLIST      0x0020

#define OPTFLAG_IS_SET       0x8000

#define OPTDOC_MASK (OPTFLAG_DOC|OPTFLAG_LIST|OPTFLAG_SUBLIST)

struct optdef {
	char *opt_name;
	char *opt_argdoc;
	int opt_flags;
	int opt_code;
	char *opt_doc;
	char *opt_env;
};

struct parseopt {
	/* User-supplied data: */
	char *po_descr;        /* Program description. */
	char *po_argdoc;       /* Argument docstring. */
	struct optdef *po_optdef; /* Option definitions. */
	char *po_extradoc;        /* Extra documentation. */

	char *po_package_name;
	char *po_package_url;
	char *po_bugreport_address;

	int po_ex_usage;       /* Exit code for usage error case. */
	void (*po_error)(char const *, ...); /* Error reporting function. */

	void (*po_setopt) (int, char *);

	/* Internal fields: */
	int *po_optidx;        /* Option indexes in lexicographical order. */
	int po_optcount;       /* Number of elements in optidx
				  (actual options). */

	/* Fields initialized by parseopt_init: */
	int po_argc;           /* Number of arguments. */
	char **po_argv;        /* Argument vector. */

	/* Fields initialized by parseopt_init and maintained by
	   parseopt_next: */
	int po_argi;           /* Index of next argument in argv[]. */
	int po_eopt;           /* End of options: if true, treat remaining
				  argv as filename arguments. */
	char const *po_optname;/* Current option. */
	int po_dash_count;     /* Is it a long option? */
	char po_opt_buf[2];

	/* The following two keep the position of the first non-optional
	   argument and the number of contiguous non-optional arguments
	   after it. Obviously, the following holds true:

	   po_arg_start + po_arg_count == po_argi
	*/
	int po_arg_start;
	int po_arg_count;
	int po_permuted;       /* Whether the arguments were permuted */

};

int parseopt_next (struct parseopt *po, char **ret_arg);
int parseopt_init (struct parseopt *po, int argc, char **argv);
void parseopt_argv (struct parseopt *po, int *argc, char ***argv);
void parseopt_getopt(struct parseopt *po, int argc, char **argv);
void parseopt_getenv(struct parseopt *po);
void parseopt_clrenv(struct parseopt *po);
void parseopt_help(struct parseopt *po);
void parseopt_usage(struct parseopt *po);

extern const char *progname;
