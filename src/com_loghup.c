/* genrc - generic system initialization script helper
   Copyright (C) 2024-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"

int
com_loghup(void)
{
	PIDLIST pids;
	size_t i;
	pid_t ppid = -1;

	pidlist_init(&pids);
	if (get_pid_list(genrc_pid_closure, &pids))
		return 1;

	for (i = 0; i < pids.pidc; i++) {
		pid_t pid = get_genrc_pid(pids.pidv[i]);
		if (pid != -1) {
			if (ppid == -1)
				ppid = pid;
			else if (ppid != pid) {
				ppid = -1;
				break;
			}
		}
	}

	pidlist_free(&pids);
	if (ppid == -1) {
		genrc_error("cannot reliably determine pid of the running process");
		return 1;
	}

	if (kill(ppid, SIGHUP)) {
		system_error(errno, "signalling %ld", (long)ppid);
		return 1;
	}

	return 0;
}
