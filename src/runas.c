/* genrc - generic system initialization script helper
   Copyright (C) 2018-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"
#include <pwd.h>
#include <grp.h>

static void
addgid(gid_t **pgv, size_t *pgc, size_t *pgi, gid_t gid)
{
	gid_t *gv = *pgv;
	size_t gc = *pgc;
	size_t gi = *pgi;

	if (gi == gc)
		gv = x2nrealloc(gv, &gc, sizeof(gv[0]));

	gv[gi++] = gid;
	*pgv = gv;
	*pgc = gc;
	*pgi = gi;
}

static int
member(gid_t *gv, size_t gc, gid_t gid)
{
	size_t i;

	for (i = 0; i < gc; i++)
		if (gv[i] == gid)
			return 1;
	return 0;
}

static size_t
get_user_groups(const char *user, gid_t **pgv, size_t *pgc)
{
	struct group *gr;
	size_t gi = 0;

	setgrent();
	while ((gr = getgrent())) {
		char **p;
		for (p = gr->gr_mem; *p; p++)
			if (strcmp(*p, user) == 0)
				addgid(pgv, pgc, &gi, gr->gr_gid);
	}
	endgrent();
	return gi;
}

static gid_t
strtogid(char const *str)
{
	struct group *gr;

	if (str[0] == '+') {
		char *end;
		unsigned long n;

		errno = 0;
		n = strtoul(str, &end, 10);
		if (errno || *end) {
			genrc_error("invalid group name %s", str);
			exit(1);
		}

		gr = getgrgid(n);
	} else
		gr = getgrnam(str);

	if (!gr) {
		genrc_error("%s: no such group", str);
		exit(1);
	}

	return gr->gr_gid;
}

void
runas(char const *runas_user, char const *runas_groups)
{
	struct passwd *pw;
	uid_t uid;
	gid_t gid;
	gid_t *gv;
	size_t gc, gn;

	if (!(runas_user || runas_groups))
		return;
	if (getuid() != 0) {
		genrc_error("not root: can't switch to user privileges");
		exit(1);
	}

	if (!runas_user) {
		pw = getpwuid(0);
		runas_user = "root";
	} else if (runas_user[0] == '+') {
		char *end;
		unsigned long n;

		errno = 0;
		n = strtoul(runas_user + 1, &end, 10);
		if (errno || *end) {
			genrc_error("invalid user name %s", runas_user);
			exit(1);
		}

		pw = getpwuid(n);
	} else
		pw = getpwnam(runas_user);

	if (!pw) {
		genrc_error("%s: no such user", runas_user);
		exit(1);
	}
	runas_user = pw->pw_name;

	uid = pw->pw_uid;
	gid = pw->pw_gid;

	gv = NULL;
	gc = 0;

	if (runas_groups && runas_groups[0]) {
		struct wordsplit ws;
		size_t i;

		ws.ws_delim = ",";
		ws.ws_error = genrc_error;
		if (wordsplit(runas_groups, &ws,
			      WRDSF_NOCMD
			      | WRDSF_NOVAR
			      | WRDSF_DELIM
			      | WRDSF_ENOMEMABRT
			      | WRDSF_SHOWERR
			      | WRDSF_ERROR))
			exit(1);

		if (ws.ws_wordc == 0) {
			genrc_error("bad group list: '%s'", runas_groups);
			exit(1);
		}

		gid = strtogid(ws.ws_wordv[0]);
		for (i = 1; i < ws.ws_wordc; i++)
			addgid(&gv, &gc, &gn, strtogid(ws.ws_wordv[i]));

		wordsplit_free(&ws);
	}

	if (gc == 0) {
		gn = get_user_groups(runas_user, &gv, &gc);
		if (!member(gv, gn, gid))
			addgid(&gv, &gc, &gn, gid);
	}

	/* Reset group permissions */
	if (setgroups(gn, gv)) {
		system_error(errno, "setgroups");
		exit(1);
	}
	free(gv);

	if (gid) {
		/* Switch to the user's gid. */
		if (setgid(gid)) {
			system_error(errno, "setgid(%lu) failed",
				     (unsigned long) gid);
			exit(1);
		}
	}

	/* Now reset uid */
	if (uid) {
		if (setuid(uid)) {
			system_error(errno, "setuid(%lu) failed: %s",
				     (unsigned long) uid);
			exit(1);
		}
	}

	setenv("HOME", pw->pw_dir, 1);
	setenv("USER", pw->pw_name, 1);
}
