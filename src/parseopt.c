/*
 * genrc - generic system initialization script helper
 * Copyright (C) 2023-2025 Sergey Poznyakoff
 *
 * Genrc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * Genrc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with genrc. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <limits.h>
#include <sysexits.h>
#include <assert.h>
#include "parseopt.h"
#include "wordwrap.h"

const char *progname;

static struct optdef *
option_find_short(struct parseopt *po, char **argptr)
{
	int i;

	for (i = 0; i < po->po_optcount; i++) {
		struct optdef *opt = &po->po_optdef[po->po_optidx[i]];
		size_t namelen = strlen(opt->opt_name);

		if (namelen == 1 && opt->opt_name[0] == po->po_optname[0]) {
			while (opt > po->po_optdef &&
			       (opt->opt_flags & OPTFLAG_ALIAS))
				opt--;
			if (opt->opt_argdoc && po->po_optname[1]) {
				*argptr = (char*) po->po_optname + 1;
				po->po_opt_buf[0] = po->po_optname[0];
				po->po_opt_buf[1] = 0;
				po->po_optname = po->po_opt_buf;
			} else
				*argptr = NULL;
			return opt;
		}
	}

	po->po_error("unrecognized option '-%c'", po->po_optname[0]);

	return NULL;
}

static struct optdef *
option_find_long(struct parseopt *po, char **argptr)
{
	size_t optlen = strcspn(po->po_optname, "=");
	int i;
	int found = 0;
	struct optdef *found_opt = NULL;

	for (i = 0; i < po->po_optcount; i++) {
		struct optdef *opt = &po->po_optdef[po->po_optidx[i]];
		size_t namelen = strlen(opt->opt_name);

		/* Ignore single-letter options when used with -- */
		if (namelen == 1)
			continue;

		if (optlen <= namelen) {
			if (memcmp(opt->opt_name, po->po_optname, optlen) == 0) {
				switch (found) {
				case 0:
					found_opt = opt;
					found++;
					if (optlen == namelen) {
						/* Exact match */
						goto end;
					}
					break;

				case 1:
					found++;
					po->po_error("option '--%*.*s' is ambiguous;"
						  " possibilities:",
						  optlen, optlen, po->po_optname);
					fprintf(stderr, "--%s\n",
						found_opt->opt_name);
					/* fall through */
				case 2:
					fprintf(stderr, "--%s\n",
						opt->opt_name);
				}
			}
			else if (found)
				break;
		}
	}
end:
	switch (found) {
	case 0:
		po->po_error("unrecognized option '--%*.*s'", optlen, optlen,
			  po->po_optname);
		break;

	case 1:
		while (found_opt > po->po_optdef &&
		       (found_opt->opt_flags & OPTFLAG_ALIAS))
			found_opt--;
		if (po->po_optname[optlen] == '=')
			*argptr = (char*) po->po_optname + optlen + 1;
		else
			*argptr = NULL;
		return found_opt;

	case 2:
		break;
	}
	return NULL;
}

static char const *dash_str[] = { "", "-", "--" };

static inline char const *
option_dash(struct optdef *opt)
{
	return dash_str[strlen(opt->opt_name) == 1 ? 1 : 2];
}

static inline int
end_of_options(struct optdef *opt)
{
	return opt->opt_name == NULL && opt->opt_flags == 0;
}

static void
permute(struct parseopt *po)
{
	/* Array to save arguments in */
	char *save[2];
	/* Number of arguments processed (at most two) */
	int n = po->po_argi - (po->po_arg_start + po->po_arg_count);
	assert(n <= 2);

	/* Store the processed elements away */
	save[0] = po->po_argv[po->po_arg_start + po->po_arg_count];
	if (n == 2)
		save[1] = po->po_argv[po->po_arg_start + po->po_arg_count + 1];

	/* Shift the array */
	memmove(po->po_argv + po->po_arg_start + n,
		po->po_argv + po->po_arg_start,
		po->po_arg_count * sizeof (po->po_argv[0]));

	/* Place saved elements in the vacating slots */
	po->po_argv[po->po_arg_start] = save[0];
	if (n == 2)
		po->po_argv[po->po_arg_start + 1] = save[1];

	/* Fix up start index */
	po->po_arg_start += n;
	po->po_permuted = 1;
}

int
parseopt_next(struct parseopt *po, char **ret_arg)
{
	char *arg, *opt_arg;
	struct optdef *opt;

	if (po->po_dash_count == 1 && po->po_optname && po->po_optname[1])
		po->po_optname++;
	else {
		permute(po);

		for (;;) {
			if (po->po_argi == po->po_argc)
				return EOF;

			arg = po->po_argv[po->po_argi++];

			po->po_dash_count = 0;
			if (po->po_eopt)
				return EOF;

			if (*arg == '-') {
				arg++;
				po->po_dash_count++;
				if (*arg == '-') {
					arg++;
					po->po_dash_count++;
					if (*arg == 0) {
						if (po->po_argi == po->po_argc) {
							permute (po);
							return EOF;
						}
						po->po_eopt = 1;
						*ret_arg = po->po_argv[po->po_argi++];
						return 0;
					}
				}
				po->po_optname = arg;
				break;
			} else {
				if (!po->po_permuted && po->po_arg_count == 0)
					po->po_arg_start = po->po_argi - 1;
				po->po_arg_count++;
			}
		}
	}

	if (po->po_dash_count) {
		opt = (po->po_dash_count == 2
		       ? option_find_long : option_find_short)(po, &opt_arg);
		if (!opt) {
			exit(po->po_ex_usage);
		}
		if (opt->opt_argdoc) {
			if (opt_arg)
				*ret_arg = opt_arg;
			else if (opt->opt_flags & OPTFLAG_ARG_OPTIONAL)
				*ret_arg = NULL;
			else if (po->po_argi == po->po_argc) {
				po->po_error("option '%s%s' requires argument",
					     dash_str[po->po_dash_count],
					     opt->opt_name);
				exit(po->po_ex_usage);
			} else
				*ret_arg = po->po_argv[po->po_argi++];
		} else if (opt_arg) {
			po->po_error("option '%s%s' does not take argument",
				     dash_str[po->po_dash_count],
				     opt->opt_name);
			exit(po->po_ex_usage);
		}
		opt->opt_flags |= OPTFLAG_IS_SET;
		return opt->opt_code;
	} else
		*ret_arg = arg;
	return EOF;
}

void
parseopt_argv(struct parseopt *po, int *argc, char ***argv)
{
	*argc = po->po_argc - po->po_arg_start;
	*argv = po->po_argv + po->po_arg_start;
}

static void
parseopt_err(char const *fmt, ...)
{
	va_list ap;
	fprintf(stderr, "%s: ", progname);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fputc('\n', stderr);
}

int
parseopt_init(struct parseopt *po, int argc, char **argv)
{
	int i, j, k;

	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;

	if (po->po_ex_usage == 0)
		po->po_ex_usage = EX_USAGE;
	if (!po->po_error)
		po->po_error = parseopt_err;

	po->po_argc = argc;
	po->po_argv = argv;
	po->po_argi = 1;
	po->po_eopt = 0;
	po->po_arg_start = 0;
	po->po_arg_count = 0;
	po->po_permuted = 0;

	/* Collect and sort actual options */
	po->po_optcount = 0;
	for (i = 0; !end_of_options(&po->po_optdef[i]); i++)
		if (!(po->po_optdef[i].opt_flags & OPTFLAG_DOC))
			po->po_optcount++;

	if (po->po_optcount == 0)
		return 0;

	po->po_optidx = calloc (po->po_optcount, sizeof (po->po_optidx[0]));
	if (po->po_optidx == NULL)
		return -1;
	for (i = 0; po->po_optdef[i].opt_flags & OPTFLAG_DOC; i++)
		;

	j = 0;
	po->po_optidx[j++] = i;
	if (po->po_optcount > 1) {
		for (i++; !end_of_options(&po->po_optdef[i]); i++) {
			if (po->po_optdef[i].opt_flags & OPTFLAG_DOC)
				continue;
			po->po_optidx[j] = i;
			for (k = j;
			     k > 0 &&
			     strcmp(po->po_optdef[po->po_optidx[k-1]].opt_name,
				    po->po_optdef[po->po_optidx[k]].opt_name) > 0;
			     k--) {
				int tmp = po->po_optidx[k];
				po->po_optidx[k] = po->po_optidx[k-1];
				po->po_optidx[k-1] = tmp;
			}
			j++;
		}
	}
	return 0;
}

void
parseopt_getopt(struct parseopt *po, int argc, char **argv)
{
	int c;
	char *p;
	assert(po->po_setopt != NULL);
	parseopt_init(po, argc, argv);
	while ((c = parseopt_next(po, &p)) != EOF)
		po->po_setopt(c, p);
}

void
parseopt_getenv(struct parseopt *po)
{
	struct optdef *opt;

	assert(po->po_setopt != NULL);
	for (opt = po->po_optdef; !end_of_options(opt); opt++) {
		char *p;
		if ((opt->opt_flags & OPTFLAG_IS_SET) || opt->opt_env == NULL)
			continue;
		if ((p = getenv(opt->opt_env)) != NULL) {
			if (!opt->opt_argdoc) {
				if (strcmp(p, "0") == 0)
					continue;
				else if (strcmp(p, "1")) {
					po->po_error("bad value of environment"
						     " variable %s: must be"
						     " 0 or 1", opt->opt_env);
					exit(po->po_ex_usage);
				}
			}
			po->po_setopt(opt->opt_code, p);
		}
	}
}

void
parseopt_clrenv(struct parseopt *po)
{
	struct optdef *opt;
	for (opt = po->po_optdef; !end_of_options(opt); opt++)
		opt->opt_env = NULL;
}

static unsigned short_opt_col = 2;
static unsigned long_opt_col = 6;
static unsigned doc_opt_col = 2;
static unsigned header_col = 1;
static unsigned opt_doc_col = 29;
static unsigned sublist_col = 29;
static unsigned sublist_doc_col = 36;
static unsigned usage_indent = 12;
static unsigned rmargin = 79;

static unsigned dup_args = 0;
static unsigned dup_args_note = 1;

enum usage_var_type {
	usage_var_column,
	usage_var_bool
};

struct usage_var_def {
	char *name;
	unsigned *valptr;
	enum usage_var_type type;
};

static struct usage_var_def usage_var[] = {
	{ "short-opt-col",   &short_opt_col,   usage_var_column },
	{ "header-col",      &header_col,      usage_var_column },
	{ "opt-doc-col",     &opt_doc_col,     usage_var_column },
	{ "usage-indent",    &usage_indent,    usage_var_column },
	{ "rmargin",         &rmargin,         usage_var_column },
	{ "dup-args",        &dup_args,        usage_var_bool },
	{ "dup-args-note",   &dup_args_note,   usage_var_bool },
	{ "long-opt-col",    &long_opt_col,    usage_var_column },
	{ "doc-opt-col",     &doc_opt_col,     usage_var_column },
	{ "sublist-col",     &sublist_col,     usage_var_column },
	{ "sublist-doc-col", &sublist_doc_col, usage_var_column },
	{ NULL }
};

static void
set_usage_var(char const *text, char **end)
{
	struct usage_var_def *p;
	int boolval = 1;
	size_t len = strcspn(text, ",=");
	char *endp;

	if (len > 3 && memcmp(text, "no-", 3) == 0) {
		text += 3;
		len -= 3;
		boolval = 0;
	}

	for (p = usage_var; p->name; p++) {
		if (strlen(p->name) == len &&
		    memcmp(p->name, text, len) == 0)
			break;
	}

	endp = (char*) text + len;
	if (p) {
		if (p->type == usage_var_bool) {
			if (*endp == '=') {
				if (progname)
					fprintf(stderr, "%s: ", progname);
				fprintf(stderr,
					"error in ARGP_HELP_FMT:"
					" improper usage of [no-]%s\n",
					p->name);
				endp = strchr(text + len, ',');
			} else
				*p->valptr = boolval;
		} else if (*endp == '=') {
			unsigned long val;

			errno = 0;
			val = strtoul(text + len + 1, &endp, 10);
			if (errno || (*endp && *endp != ',')) {
				if (progname)
					fprintf (stderr, "%s: ", progname);
				fprintf (stderr,
					 "error in ARGP_HELP_FMT:"
					 " bad value for %s",
					 p->name);
				if (endp)
					fprintf(stderr, " (near %s)", endp);
				fputc('\n', stderr);
			} else if (val > UINT_MAX) {
				if (progname)
					fprintf(stderr, "%s: ", progname);
				fprintf(stderr,
					"error in ARGP_HELP_FMT:"
					" %s value is out of range\n",
					p->name);
			} else
				*p->valptr = val;
		} else {
			if (progname)
				fprintf(stderr, "%s: ", progname);
			fprintf(stderr,
				"%s: ARGP_HELP_FMT parameter"
				" requires a value\n",
				p->name);
		}
	} else {
		if (progname)
			fprintf(stderr, "%s: ", progname);
		fprintf(stderr,
			"%s: Unknown ARGP_HELP_FMT parameter\n",
			text);
	}
	*end = endp;
}

static void
init_usage_vars(void)
{
	char *fmt, *p;

	fmt = getenv("ARGP_HELP_FMT");
	if (!fmt || !*fmt)
		return;

	while (1) {
		set_usage_var(fmt, &p);
		if (*p == 0)
			break;
		else if (*p == ',')
			p++;
		else {
			if (progname)
				fprintf(stderr, "%s: ", progname);
			fprintf(stderr,
				"ARGP_HELP_FMT: missing delimiter near %s\n",
				p);
			break;
		}
		fmt = p;
	}
}


static inline void
print_arg(WORDWRAP_FILE wf, struct optdef *opt, int delim, int *argsused)
{
	if (opt->opt_argdoc) {
		*argsused = 1;
		wordwrap_printf(wf, "%c%s", delim, opt->opt_argdoc);
	}
}

static inline struct optdef *
opt_unalias(struct optdef *opt)
{
	while (opt->opt_flags & OPTFLAG_ALIAS)
		opt--;
	return opt;
}

void
parseopt_usage(struct parseopt *po)
{
	int i;
	struct optdef *opt, *orig;
	WORDWRAP_FILE wf;

	init_usage_vars();
	wf = wordwrap_fdopen(1);
	wordwrap_set_right_margin(wf, rmargin);
	wordwrap_printf(wf, "%s %s ", "Usage:", progname);
	wordwrap_next_left_margin(wf, usage_indent);

	/* Print short options without arguments */
	for (i = 0; i < po->po_optcount; i++) {
		opt = &po->po_optdef[po->po_optidx[i]];
		if (opt->opt_name &&
		    opt->opt_name[0] != 0 && opt->opt_name[1] == 0 &&
		    opt->opt_argdoc == NULL &&
		    !(opt->opt_flags & (OPTFLAG_DOC|OPTFLAG_HIDDEN)))
			break;
	}

	if (i < po->po_optcount) {
		wordwrap_puts(wf, "[-");
		wordwrap_puts(wf, opt->opt_name);
		while (++i < po->po_optcount) {
			opt = &po->po_optdef[po->po_optidx[i]];
			if (opt->opt_name &&
			    opt->opt_name[0] != 0 && opt->opt_name[1] == 0 &&
			    opt->opt_argdoc == NULL &&
			    !(opt->opt_flags & (OPTFLAG_DOC|OPTFLAG_HIDDEN)))
				wordwrap_puts(wf, opt->opt_name);
		}
		wordwrap_putc(wf, ']');
	}

	/* Print short options with arguments */
	for (i = 0; i < po->po_optcount; i++) {
		opt = &po->po_optdef[po->po_optidx[i]];
		orig = opt_unalias(opt);
		if (opt->opt_name &&
		    opt->opt_name[0] != 0 && opt->opt_name[1] == 0 &&
		    orig->opt_argdoc != NULL &&
		    !(orig->opt_flags & (OPTFLAG_DOC|OPTFLAG_HIDDEN))) {
			wordwrap_word_start(wf);
			wordwrap_puts(wf, " [-");
			wordwrap_puts(wf, opt->opt_name);
			wordwrap_putc(wf, ' ');
			wordwrap_puts (wf, orig->opt_argdoc);
			wordwrap_putc(wf, ']');
			wordwrap_word_end (wf);
		}
	}

	/* Print long optons */
	for (i = 0; i < po->po_optcount; i++) {
		opt = &po->po_optdef[po->po_optidx[i]];
		orig = opt_unalias(opt);
		if (opt->opt_name &&
		    opt->opt_name[0] != 0 && opt->opt_name[1] != 0 &&
		    !(opt->opt_flags & (OPTFLAG_DOC|OPTFLAG_HIDDEN))) {
			wordwrap_word_start(wf);
			wordwrap_write(wf, " [--", 4);
			wordwrap_puts(wf, opt->opt_name);
			if (orig->opt_argdoc) {
				wordwrap_putc(wf, '=');
				if (opt->opt_flags & OPTFLAG_ARG_OPTIONAL)
					wordwrap_putc(wf, '[');
				wordwrap_puts(wf, orig->opt_argdoc);
				if (opt->opt_flags & OPTFLAG_ARG_OPTIONAL)
					wordwrap_putc(wf, ']');
			}
			wordwrap_putc(wf, ']');
			wordwrap_word_end(wf);
		}
	}
}

struct help_context {
	int argsused;
	int prev_doc;
};

static struct optdef *
print_option(WORDWRAP_FILE wf, struct optdef *opt, struct help_context *ctx)
{
	struct optdef *cur_opt, *next_opt;
	int delim;
	int w;

	/* Mark the doc entry as list if its opt_name is set. */
	if ((opt->opt_flags & OPTFLAG_DOC) && opt->opt_name)
		opt->opt_flags |= OPTFLAG_LIST;
	/* Insert a newline between two contiguous plain doc entries,
	   and between two entries of different doc type. */
	if ((ctx->prev_doc != opt->opt_flags) ||
	    (ctx->prev_doc == OPTFLAG_DOC && (opt->opt_flags & OPTFLAG_DOC) == OPTFLAG_DOC))
		wordwrap_putc(wf, '\n');
	/* Save doc flags for use in the next call. */
	ctx->prev_doc = opt->opt_flags & OPTDOC_MASK;

	/* Handle a doc entry */
	if (opt->opt_flags & OPTFLAG_DOC) {
		wordwrap_set_right_margin(wf, rmargin);
		if (opt->opt_name) {
			wordwrap_set_left_margin(wf,
				 (opt->opt_flags & OPTFLAG_SUBLIST)
				   ? sublist_col
				   : short_opt_col);
			wordwrap_puts(wf, opt->opt_name);
			wordwrap_set_left_margin(wf,
				 (opt->opt_flags & OPTFLAG_SUBLIST)
				   ? sublist_doc_col
				   : opt_doc_col);
		} else {
			wordwrap_set_left_margin(wf,
				 (opt->opt_flags & OPTFLAG_SUBLIST)
				   ? sublist_col
				   : doc_opt_col);
		}
		if (opt->opt_doc) {
			wordwrap_puts(wf, opt->opt_doc);
			if (ctx->prev_doc)
				wordwrap_putc(wf, '\n');
		}
		return opt + 1;
	}

	/* A regular option. */

	wordwrap_set_left_margin(wf, short_opt_col);

	cur_opt = opt;

	/* Short options */
	w = 0;
	do {
		if (opt->opt_name[1] == 0) {
			if (w)
				wordwrap_write(wf, ", ", 2);
			wordwrap_puts(wf, option_dash(opt));
			wordwrap_puts(wf, opt->opt_name);
			delim = ' ';
			if (dup_args)
				print_arg(wf, cur_opt, delim, &ctx->argsused);
			w = 1;
		}
	} while (!end_of_options(++opt) && (opt->opt_flags & OPTFLAG_ALIAS));
	next_opt = opt;

	/* Long options */
	for (opt = cur_opt; opt != next_opt; opt++) {
		if (opt->opt_name[1]) {
			if (w)
				wordwrap_write(wf, ", ", 2);
			wordwrap_set_left_margin(wf, long_opt_col);
			w = 0;
			break;
		}
	}

	for (; opt != next_opt; opt++) {
		if (opt->opt_name[1]) {
			if (w)
				wordwrap_write(wf, ", ", 2);
			wordwrap_printf(wf, "%s%s", option_dash(opt),
					opt->opt_name);
			delim = '=';
			if (dup_args)
				print_arg(wf, cur_opt, delim, &ctx->argsused);
			w = 1;
		}
	}

	if (!dup_args)
		print_arg(wf, cur_opt, delim, &ctx->argsused);

	wordwrap_set_left_margin(wf, opt_doc_col);
	if (cur_opt->opt_doc[0]) {
		wordwrap_puts(wf, cur_opt->opt_doc);
		wordwrap_putc(wf, '\n');
	}

	return next_opt;
}

static char dup_args_note_text[] =
  "Mandatory or optional arguments to long options are also mandatory or "
  "optional for any corresponding short options.";

void
parseopt_help(struct parseopt *po)
{
	WORDWRAP_FILE wf;
	struct optdef *opt;
	struct help_context ctx = { 0, 0 };

	init_usage_vars ();

	wf = wordwrap_fdopen(1);

	wordwrap_printf(wf, "usage: %s %s\n", progname, po->po_argdoc);

	wordwrap_set_right_margin (wf, rmargin);
	if (po->po_descr)
		wordwrap_puts(wf, po->po_descr);
	wordwrap_para(wf);

	if (po->po_optdef) {
		ctx.prev_doc = po->po_optdef->opt_flags & OPTDOC_MASK;
		for (opt = po->po_optdef; !end_of_options(opt);
		     opt = print_option(wf, opt, &ctx)) ;
		wordwrap_para(wf);
	}

	if (ctx.argsused && dup_args_note) {
		wordwrap_set_left_margin(wf, 0);
		wordwrap_set_right_margin(wf, rmargin);
		wordwrap_puts(wf, dup_args_note_text);
		wordwrap_para(wf);
	}

	//if (parseopt_help_hook)
	// parseopt_help_hook (stdout);//FIXME

	wordwrap_set_left_margin(wf, 0);
	wordwrap_set_right_margin(wf, rmargin);

	if (po->po_bugreport_address)
		wordwrap_printf(wf,
				"Report bugs and suggestions to <%s>\n",
				po->po_bugreport_address);
	if (po->po_package_name && po->po_package_url)
		wordwrap_printf(wf,
				"%s home page: <%s>\n", po->po_package_name,
				po->po_package_url);
}
