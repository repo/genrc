/* genrc - generic system initialization script helper
   Copyright (C) 2024-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"

int
com_wait(void)
{
	PIDLIST pids;
	struct timeval start, stop;

	gettimeofday(&start, NULL);
	pidlist_init(&pids);
	gettimeofday(&stop, NULL);
	stop.tv_sec += genrc_stop_timeout;
	for (;;) {
		if (get_pid_list(genrc_pid_closure, &pids))
			return 1;
		if (pids.pidc == 0)
			break;
		gettimeofday(&start, NULL);
		if (timercmp(&start, &stop, >=)) {
			pidlist_free(&pids);
			if (genrc_verbose) {
				printf("timed out waiting for %s to terminate\n",
				       genrc_program);
				if (pids.pidc > 1)
					printf("%zu processes still running\n",
					       pids.pidc);
			}
			return 1;
		}
		sleep(1);
	}

	pidlist_free(&pids);

	if (genrc_verbose)
		printf("%s terminated\n", genrc_program);

	return 0;
}
