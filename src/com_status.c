/* genrc - generic system initialization script helper
   Copyright (C) 2018-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"

int
com_status(void)
{
	PIDLIST pids;
	int rc;

	pidlist_init(&pids);
	rc = get_pid_list(genrc_pid_closure, &pids);
	if (rc == 0) {
		if (genrc_verbose == 0)
			return pids.pidc == 0;
		if (pids.pidc == 0) {
			printf("%s is not running\n", genrc_program);
			return 1;
		} else {
			printf("%s is running ", genrc_program);
			if (pids.pidc == 1) {
				printf("(pid %lu)",
				       (unsigned long) pids.pidv[0]);
			} else {
				int i;
				int delim = ' ';
				printf("(pids");
				for (i = 0; i < pids.pidc; i++) {
					printf("%c%lu", delim,
					       (unsigned long) pids.pidv[i]);
					delim = ',';
				}
				putchar(')');
			}
			putchar('\n');
		}
	} else {
		if (genrc_verbose)
			printf("status unknown\n");
		return 2;
	}
	return 0;
}
