/* genrc - generic system initialization script helper
   Copyright (C) 2018-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"
#include "parseopt.h"
#include <syslog.h>
#include <sys/ioctl.h>
#include <sys/resource.h>

char *genrc_command;
char *genrc_program;
char *genrc_pid_from;
unsigned genrc_start_timeout = 5;
unsigned genrc_stop_timeout = 5;
int genrc_no_reload;
int genrc_signal_stop = SIGTERM;
int genrc_signal_reload = SIGHUP;
enum genrc_kill_mode genrc_kill_mode = genrc_kill_process;
GENRC_PID_CLOSURE *genrc_pid_closure;
char *genrc_create_pidfile;
int genrc_verbose = 1;
char *genrc_shell = SHELL;
int genrc_log_facility = LOG_DAEMON;
char *genrc_log_tag = NULL;
char *genrc_user;
char *genrc_groups;
char *genrc_log_err_file;
char *genrc_log_out_file;
char *genrc_log_file;
char *genrc_timeout;
int genrc_sentinel;

enum {
	OPT_CHDIR = 256,
	OPT_COMMAND,
	OPT_CREATE_PIDFILE,
	OPT_EXEC,
	OPT_GROUP,
	OPT_HELP,
	OPT_KILL_MODE,
	OPT_LIMIT,
	OPT_LOG_ERR_FILE,
	OPT_LOG_FACILITY,
	OPT_LOG_FILE,
	OPT_LOG_OUT_FILE,
	OPT_LOG_TAG,
	OPT_NO_ENV,
	OPT_NO_RELOAD,
	OPT_PID_FILE,
	OPT_PID_FROM,
	OPT_PROGRAM,
	OPT_QUIET,
	OPT_RESTART_ON_EXIT,
	OPT_RESTART_ON_SIGNAL,
	OPT_SENTINEL,
	OPT_SHELL,
	OPT_SIGNAL_RELOAD,
	OPT_SIGNAL_STOP,
	OPT_TIMEOUT,
	OPT_USAGE,
	OPT_USER,
	OPT_VERBOSE,
	OPT_VERSION,
};

struct optdef options[] = {
	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "COMMANDs are:"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "start",
		.opt_doc = "start the program"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "stop",
		.opt_doc = "stop the program"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "restart",
		.opt_doc = "restart the running program"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "reload",
		.opt_doc = "send a reload signal to the program"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "status",
		.opt_doc = "display the program status"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "loghup",
		.opt_doc = "send HUP to the logger process"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "wait",
		.opt_doc = "wait for the program to terminate"
	},

	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "Command to run:"
	},

	{
		.opt_name = "c",
		.opt_argdoc = "COMMAND",
		.opt_code = OPT_COMMAND,
		.opt_doc = "command line to run",
		.opt_env = ENV_GENRC_COMMAND,
	},
	{
		.opt_name = "command",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "p",
		.opt_argdoc = "PROGRAM",
		.opt_code = OPT_PROGRAM,
		.opt_doc = "name of the program to run",
		.opt_env = ENV_GENRC_PROGRAM
	},
	{
		.opt_name = "program",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc =
		"At least one of COMMAND or PROGRAM must be given."
		" If PROGRAM is supplied, but COMMAND is not, COMMAND"
		" is set to PROGRAM."
		" Otherwise, if COMMAND is set, but PROGRAM is not,"
		" PROGRAM is set to the"
		" first word (in the shell sense) in COMMAND."
	},
	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "Runtime privileges and limits:"
	},

	{
		.opt_name = "u",
		.opt_argdoc = "NAME",
		.opt_code = OPT_USER,
		.opt_doc = "run with this user privileges",
		.opt_env = ENV_GENRC_USER,
	},
	{
		.opt_name = "user",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "g",
		.opt_argdoc = "GROUP[,GROUP...]",
		.opt_code = OPT_GROUP,
		.opt_doc = "run with this group(s) privileges",
		.opt_env = ENV_GENRC_GROUP
	},
	{
		.opt_name = "group",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "l",
		.opt_argdoc = "LIM",
		.opt_code = OPT_LIMIT,
		.opt_doc = "set resource limit; LIM is a resource letter"
		" followed by the resource value;"
		" resource letters are:"
	},
	{
		.opt_name = "limit",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "c",
		.opt_doc = "core file size (KB)"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "d",
		.opt_doc = "data size (KB)"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "f",
		.opt_doc = "file size (KB)"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "l",
		.opt_doc = "locked-in-memory address space (KB)"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "m",
		.opt_doc = "resident set size (KB)"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "n",
		.opt_doc = "number of open files",
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "p",
		.opt_doc = "process priority -20..19"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "s",
		.opt_doc = "stack size (KB)"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "t",
		.opt_doc = "CPU time (seconds)",
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "t",
		.opt_doc = "CPU time (seconds)",
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "u",
		.opt_doc = "number of subprocesses",
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "v",
		.opt_doc = "virtual memory (KB)",
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_doc =
		"Use upper-case letters to enforce limit setting. "
		"Several limits can be set in one option, e.g.: "
		"-l n128P-10t60"
	},

	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "Additional configuration:"
	},

	{
		.opt_name = "C",
		.opt_argdoc = "DIR",
		.opt_code = OPT_CHDIR,
		.opt_doc = "change to DIR",
	},
	{
		.opt_name = "directory",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "no-env",
		.opt_code = OPT_NO_ENV,
		.opt_doc = "don't read settings from environment variables"
	},

	{
		.opt_name = "k",
		.opt_argdoc = "MODE",
		.opt_code = OPT_KILL_MODE,
		.opt_doc = "set program termination mode: group, program,"
		" or mixed",
	},
	{
		.opt_name = "kill-mode",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "t",
		.opt_argdoc = "SECONDS",
		.opt_code = OPT_TIMEOUT,
		.opt_doc = "time to wait for the program to start up or"
		" terminate; to set timeouts separately, use"
		" start=N or stop=N as argument; the two can be"
		" specified together, as in: start=N,stop=N",
		.opt_env = ENV_GENRC_TIMEOUT
	},
	{
		.opt_name = "timeout",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "P",
		.opt_argdoc = "SOURCE",
		.opt_code = OPT_PID_FROM,
		.opt_doc = "where to look for PIDs of the running programs",
		.opt_env = ENV_GENRC_PID_FROM
	},
	{
		.opt_name = "pid-from",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "F",
		.opt_argdoc = "NAME",
		.opt_code = OPT_PID_FILE,
		.opt_doc = "name of the PID file"
		" (same as --pid-from=FILE:NAME)"
	},
	{
		.opt_name = "pidfile",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "q",
		.opt_code = OPT_QUIET,
		.opt_doc = "suppress normal diagnostic messages",
	},

	{
		.opt_name = "signal-reload",
		.opt_argdoc = "SIG",
		.opt_code = OPT_SIGNAL_RELOAD,
		.opt_doc = "signal to send on reload (default: SIGHUP);"
		" setting it to 0 is equivalent to --no-reload",
		.opt_env = ENV_GENRC_SIGNAL_RELOAD
	},

	{
		.opt_name = "no-reload",
		.opt_code = OPT_NO_RELOAD,
		.opt_doc = "makes reload equivalent to restart"
		" (default: SIGTERM)",
	},

	{
		.opt_name = "v",
		.opt_code = OPT_VERBOSE,
		.opt_doc = "enable verbose diagnostics",
	},
	{
		.opt_name = "verbose",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "Sentinel mode:"
	},

	{
		.opt_name = "sentinel",
		.opt_code = OPT_SENTINEL,
		.opt_doc = "PROGRAM runs in foreground; disconnect from the"
		" controlling terminal, run it and act as a sentinel",
		.opt_env = ENV_GENRC_SENTINEL
	},
	{
		.opt_name = "S",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "create-pidfile",
		.opt_argdoc = "FILE",
		.opt_code = OPT_CREATE_PIDFILE,
		.opt_doc = "store PID of the PROGRAM in FILE; implies"
		" --pid-from=FILE:FILENAME, unless --pid-from is also given"
	},

	{
		.opt_name = "shell",
		.opt_argdoc = "SHELL",
		.opt_code = OPT_SHELL,
		.opt_doc = "use SHELL instead of /bin/sh;"
		" --shell=none to exec PROGRAM directly",
	},
	{
		.opt_name = "s",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "exec",
		.opt_code = OPT_EXEC,
		.opt_doc = "same as --shell=none",
	},
	{
		.opt_name = "e",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "restart-on-exit",
		.opt_argdoc = "[!]CODE[,...]",
		.opt_code = OPT_RESTART_ON_EXIT,
		.opt_doc = "restart the program if it exits with one of the"
		" listed status codes",
	},

	{
		.opt_name = "restart-on-signal",
		.opt_argdoc = "[!]SIG[,...]",
		.opt_code = OPT_RESTART_ON_SIGNAL,
		.opt_doc = "restart the program if it terminates on one of the"
		" listed signals",
	},

	{
		.opt_name = "log-facility",
		.opt_argdoc = "FACILITY",
		.opt_code = OPT_LOG_FACILITY,
		.opt_doc = "log diagnostic messages to this syslog facility",
		.opt_env = ENV_GENRC_LOG_FACILITY
	},

	{
		.opt_name = "log-tag",
		.opt_argdoc = "TAG",
		.opt_code = OPT_LOG_TAG,
		.opt_doc = "use this syslog tag instead of the program name",
		.opt_env = ENV_GENRC_LOG_TAG
	},

	{
		.opt_name = "log-file",
		.opt_argdoc = "FILE",
		.opt_code = OPT_LOG_FILE,
		.opt_doc = "send command's stdout and stderr to FILE"
	},

	{
		.opt_name = "log-err-file",
		.opt_argdoc = "FILE",
		.opt_code = OPT_LOG_ERR_FILE,
		.opt_doc = "send command's stderr to FILE",
		.opt_env = ENV_GENRC_LOG_ERR_FILE
	},

	{
		.opt_name = "log-out-file",
		.opt_argdoc = "FILE",
		.opt_code = OPT_LOG_OUT_FILE,
		.opt_doc = "send command's stdout to FILE",
		.opt_env = ENV_GENRC_LOG_OUT_FILE
	},

	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "Informational options:"
	},

	{
		.opt_name = "help",
		.opt_code = OPT_HELP,
		.opt_doc = "display this help list",
	},
	{
		.opt_name = "h",
		.opt_flags = OPTFLAG_ALIAS,
	},

	{
		.opt_name = "usage",
		.opt_code = OPT_USAGE,
		.opt_doc = "display short usage information",
	},

	{
		.opt_name = "version",
		.opt_code = OPT_VERSION,
		.opt_doc = "display program version and exit",
	},

	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "Influential environment variables and corresponding options:"
	},

	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_COMMAND "=COMMAND",
		.opt_doc = "--command=COMMAND",
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_GROUP "=GROUPS",
		.opt_doc = "--group=GROUPS"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_KILL_MODE "=MODE",
		.opt_doc = "--kill-mode=MODE"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_LOG_ERR_FILE  "=FILE",
		.opt_doc = "--log-err-file=FILE"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_LOG_FACILITY  "=F",
		.opt_doc = "--log-facility=F"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_LOG_FILE "=FILE",
		.opt_doc = "--log-file=FILE"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_LOG_OUT_FILE  "=FILE",
		.opt_doc = "--log-out-file=FILE"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_LOG_TAG "=STR",
		.opt_doc = "--log-tag=STR"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_PID_FROM "=SOURCE",
		.opt_doc = "--pid-from=SOURCE"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_PROGRAM "=NAME",
		.opt_doc = "--program=NAME",
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_SENTINEL "=1",
		.opt_doc = "--sentinel"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_TIMEOUT "=SECONDS",
		.opt_doc = "--timeout=SECONDS",
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = ENV_GENRC_USER "=NAME",
		.opt_doc = "--user=NAME"
	},

	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "PID sources:"
	},

	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name =	"FILE:<NAME>",
		.opt_doc = "Read PID from the file <NAME>"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "CONFIG:<LANG>:<FILENAME>:<FQRN>",
		.opt_doc =
		"Name of the PID file is stored in relation <FQRN> of"
		" the configuration file <FILENAME>, written in language"
		" <LANG>"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "GREP:<FILE>:s/<RX>/<REPL>/[<FLAGS>][;...]",
		.opt_doc = "Grep for the first line in <FILE> that"
		" matches <RX>. If found, replace the matched portion"
		" according to <REPL> and <FLAGS>. Use the resulting"
		" string as PID. Several sed expressions can be supplied",
		" separated with semicolons."
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "PROC[:[<EXE>][:<FLAGS>]]",
		.opt_doc = "Look for matching program in /proc/<PID>/*."
		" If <EXE> is not supplied or empty, program name from"
		" --program will be used. <FLAGS> are:"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "e",
		.opt_doc = "exact match"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "g",
		.opt_doc = "glob pattern match"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "x",
		.opt_doc = "extended POSIX regexp match (default)"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "p",
		.opt_doc = "PCRE match"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "i",
		.opt_doc = "case-insensitive match"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "c",
		.opt_doc = "match entire command line"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "r",
		.opt_doc = "match real executable name (instead of argv0)"
	},
	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_SUBLIST,
		.opt_name = "a",
		.opt_doc = "signal all matching PIDs",
	},

	{
		.opt_flags = OPTFLAG_DOC|OPTFLAG_LIST,
		.opt_name = "PS[:[<EXE>][:<FLAGS>]]",
		.opt_doc = "Look for matching program in the output"
		" of 'ps -efw'. <EXE> and <FLAGS> are as described above"
	},

	{
		.opt_flags = OPTFLAG_DOC,
		.opt_doc = "Default PID source: " DEFAULT_PID_SOURCE
	},

	{ NULL }
};

static void genrc_setopt(int c, char *arg);

struct parseopt po = {
	.po_descr = "generic system initialization script helper",
	.po_argdoc = "[OPTIONS] COMMAND",
	.po_optdef = options,
	//.po_extradoc =

	.po_package_name = PACKAGE_NAME,
	.po_package_url = PACKAGE_URL,
	.po_bugreport_address = PACKAGE_BUGREPORT,

	.po_ex_usage = 1,
	.po_error = usage_error,

	.po_setopt = genrc_setopt
};

struct sigdefn {
	char const *sig_name;
	int sig_no;
};

#define S(s) { #s, s }
static struct sigdefn sigdefn[] = {
  S (SIGHUP),
  S (SIGINT),
  S (SIGQUIT),
  S (SIGILL),
  S (SIGTRAP),
  S (SIGABRT),
  S (SIGIOT),
  S (SIGBUS),
  S (SIGFPE),
  S (SIGKILL),
  S (SIGUSR1),
  S (SIGSEGV),
  S (SIGUSR2),
  S (SIGPIPE),
  S (SIGALRM),
  S (SIGTERM),
#ifdef SIGSTKFLT
  S (SIGSTKFLT),
#endif
  S (SIGCHLD),
  S (SIGCONT),
  S (SIGSTOP),
  S (SIGTSTP),
  S (SIGTTIN),
  S (SIGTTOU),
#ifdef SIGURG
  S (SIGURG),
#endif
#ifdef SIGXCPU
  S (SIGXCPU),
#endif
#ifdef SIGXFSZ
  S (SIGXFSZ),
#endif
#ifdef SIGVTALRM
  S (SIGVTALRM),
#endif
#ifdef SIGPROF
  S (SIGPROF),
#endif
#ifdef SIGWINCH
  S (SIGWINCH),
#endif
#ifdef SIGPOLL
  S (SIGPOLL),
#endif
#ifdef SIGIO
  S (SIGIO),
#endif
#ifdef SIGPWR
  S (SIGPWR),
#endif
#ifdef SIGSYS
  S (SIGSYS),
#endif
  {NULL}
};
#undef S

static int
is_numeric_str(char const *s)
{
	while (*s) {
		if (!isdigit(*s))
			return 0;
		s++;
	}
	return 1;
}

int
str_to_int(char const *s)
{
	char *end;
	unsigned long n;
	errno = 0;
	n = strtoul(s, &end, 10);
	if (errno || *end || n > UINT_MAX)
		return -1;
	return n;
}

int
str_to_sig(char const *s)
{
	if (is_numeric_str(s)) {
		return str_to_int(s);
	} else {
		struct sigdefn *sd;

		for (sd = sigdefn; sd->sig_name; sd++) {
			if (s[0] == 's' || s[0] == 'S') {
				if (strcasecmp(sd->sig_name, s) == 0)
					return sd->sig_no;
			} else if (strcasecmp(sd->sig_name + 3, s) == 0)
				return sd->sig_no;
		}
	}
	return -1;
}

enum {
	LIM_PRIO,
	LIM_CORE,
	LIM_DATA,
	LIM_FSIZE,
	LIM_MEMLOCK,
	LIM_RSS,
	LIM_NOFILE,
	LIM_STACK,
	LIM_CPU,
	LIM_NPROC,
	LIM_AS,

	NLIM
};

static struct genrc_resource {
	int letter;
	int resource;
	long mul;
	int isset;
	int force;
	long value;
} resource_tab[] = {
	{ 'p', 0, 0 },
	{ 'c', RLIMIT_CORE, 1024 },
	{ 'd', RLIMIT_DATA, 1024 },
	{ 'f', RLIMIT_FSIZE, 1024  },
	{ 'l', RLIMIT_MEMLOCK, 1024 },
	{ 'm', RLIMIT_RSS, 1024 },
	{ 'n', RLIMIT_NOFILE, 1 },
	{ 's', RLIMIT_STACK, 1024 },
	{ 't', RLIMIT_CPU, 1 },
	{ 'u', RLIMIT_NPROC, 1 },
	{ 'v', RLIMIT_AS, 1024},
};

void
setlimits(void)
{
	int i;

	for (i = 0; i < NLIM; i++) {
		if (resource_tab[i].isset) {
			if (i == LIM_PRIO) {
				if (setpriority(PRIO_PROCESS, 0,
						resource_tab[i].value)) {
					genrc_error("failed to set process priority: %s",
						    strerror(errno));
					if (resource_tab[i].force)
						_exit(126);
				}
			} else {
				struct rlimit rlim;

				rlim.rlim_cur = rlim.rlim_max = resource_tab[i].value;
				if (setrlimit(resource_tab[i].resource, &rlim)) {
					genrc_error("%c: failed to set limit: %s",
						    resource_tab[i].letter,
						    strerror(errno));
					if (resource_tab[i].force)
						_exit(126);
				}
			}
		}
	}
}

static int
find_resource(int letter)
{
	int i;
	letter = tolower(letter);
	for (i = 0; i < sizeof(resource_tab)/sizeof(resource_tab[0]); i++)
		if (letter == resource_tab[i].letter)
			return i;
	return -1;
}

static void
parse_limits(char *arg)
{
	int r;
	char *p;
	long v;

	for (;;) {
		while (*arg == ' ' || *arg == '\t')
			arg++;
		if (*arg == 0)
			break;

		if ((r = find_resource(arg[0])) == -1) {
			usage_error("%s: unrecognized resource code", arg);
		}

		errno = 0;
		v = strtol(arg + 1, &p, 10);
		if (errno) {
			usage_error("%s: resource value out of range", arg);
		}

		if (r == LIM_PRIO) {
			if (v < -20 || v > 19)
				usage_error("%s: resource value out of range", arg);
		} else {
			if (v < 0)
				usage_error("%s: resource value out of range", arg);
			v *= resource_tab[r].mul;
		}
		resource_tab[r].isset = 1;
		resource_tab[r].force = isupper(arg[0]);
		resource_tab[r].value = v;
		arg = p;
	}
}

static const char gplv3[] =
	"License GPLv3+: GNU GPL version 3 or later "
	"<http://gnu.org/licenses/gpl.html>\n"
	"This is free software: you are free to change and redistribute it.\n"
	"There is NO WARRANTY, to the extent permitted by law.\n\n";

void
version(void)
{
	printf("%s\n", PACKAGE_STRING);
	printf("Copyright (C) 2018-2025 Sergey Poznyakoff\n");
	printf("%s", gplv3);
}

static struct facility_def {
	char const *name;
	int facility;
} facility_tab[] = {
	{ "auth",    LOG_AUTH },
	{ "authpriv",LOG_AUTHPRIV },
	{ "cron",    LOG_CRON },
	{ "daemon",  LOG_DAEMON },
	{ "ftp",     LOG_FTP },
	{ "lpr",     LOG_LPR },
	{ "mail",    LOG_MAIL },
	{ "user",    LOG_USER },
	{ "local0",  LOG_LOCAL0 },
	{ "local1",  LOG_LOCAL1 },
	{ "local2",  LOG_LOCAL2 },
	{ "local3",  LOG_LOCAL3 },
	{ "local4",  LOG_LOCAL4 },
	{ "local5",  LOG_LOCAL5 },
	{ "local6",  LOG_LOCAL6 },
	{ "local7",  LOG_LOCAL7 },
	{ NULL }
};

static int
str_to_facility(char const *str)
{
	int i;
	for (i = 0; facility_tab[i].name; i++)
		if (strcasecmp(str, facility_tab[i].name) == 0)
			return facility_tab[i].facility;
	return -1;
}

void
genrc_openlog(void)
{
	openlog(genrc_log_tag, LOG_PID, genrc_log_facility);
}

typedef int (*GENRC_COMMAND)(void);

struct comtab {
	char const *com_name;
	GENRC_COMMAND com_fun;
};

static struct comtab comtab[] = {
	{ "start",   com_start },
	{ "stop",    com_stop },
	{ "restart", com_restart },
	{ "reload",  com_reload },
	{ "status",  com_status },
	{ "loghup",  com_loghup },
	{ "wait",    com_wait },
	{ NULL, NULL }
};

GENRC_COMMAND
find_command(char const *s)
{
	struct comtab *c;
	for (c = comtab; c->com_name; c++)
		if (strcmp(c->com_name, s) == 0)
			break;
	return c->com_fun;
}

extern char **environ;

static void
genrc_setopt(int c, char *arg)
{
	switch (c) {
	case OPT_HELP:
		parseopt_help(&po);
		exit(0);

	case OPT_USAGE:
		parseopt_usage(&po);
		exit(0);

	case OPT_VERSION:
		version();
		exit(0);

	case OPT_CHDIR:
		if (chdir(arg)) {
			genrc_error("%s: %s", arg, strerror(errno));
			exit(1);
		}
		break;

	case OPT_COMMAND:
		genrc_command = xstrdup(arg);
		break;

	case OPT_EXEC:
		genrc_shell = NULL;
		break;

	case OPT_NO_ENV:
		parseopt_clrenv(&po);
		break;

	case OPT_KILL_MODE:
		if (strcmp(arg, "group") == 0)
			genrc_kill_mode = genrc_kill_group;
		else if (strcmp(arg, "process") == 0)
			genrc_kill_mode = genrc_kill_process;
		else if (strcmp(arg, "mixed") == 0)
			genrc_kill_mode = genrc_kill_mixed;
		else
			usage_error("%s: invalid kill mode", arg);
		break;

	case OPT_LIMIT:
		parse_limits(arg);
		break;

	case OPT_SHELL:
		if (strcmp(arg, "no") == 0 || strcmp(arg, "none") == 0)
			genrc_shell = NULL;
		else
			genrc_shell = xstrdup(arg);
		break;

	case OPT_PROGRAM:
		genrc_program = xstrdup(arg);
		break;

	case OPT_PID_FROM:
		genrc_pid_closure = get_pid_closure(arg);
		break;

	case OPT_PID_FILE:
		genrc_pid_closure = get_pid_closure_file(arg);
		break;

	case OPT_GROUP:
		genrc_groups = xstrdup(arg);
		break;

	case OPT_QUIET:
		genrc_verbose = 0;
		break;

	case OPT_CREATE_PIDFILE:
		genrc_create_pidfile = xstrdup(arg);
		break;

	case OPT_LOG_FACILITY:
		if ((c = str_to_facility(arg)) == -1)
			usage_error("%s: unknown facility", arg);
		genrc_log_facility = c;
		break;

	case OPT_LOG_TAG:
		genrc_log_tag = xstrdup(arg);
		break;

	case OPT_LOG_ERR_FILE:
		genrc_log_err_file = xstrdup(arg);
		break;

	case OPT_LOG_OUT_FILE:
		genrc_log_out_file = xstrdup(arg);
		break;

	case OPT_LOG_FILE:
		genrc_log_file = xstrdup(arg);
		break;

	case OPT_TIMEOUT:
		genrc_timeout = arg;
		break;

	case OPT_SENTINEL:
		genrc_sentinel = 1;
		break;

	case OPT_RESTART_ON_EXIT:
		add_restart_condition(RESTART_ON_EXIT, arg);
		break;

	case OPT_RESTART_ON_SIGNAL:
		add_restart_condition(RESTART_ON_SIGNAL, arg);
		break;

	case OPT_NO_RELOAD:
		genrc_no_reload = 1;
		break;

	case OPT_SIGNAL_RELOAD:
		genrc_signal_reload = str_to_sig(arg);
		if (genrc_signal_reload == -1)
			usage_error("%s: invalid signal number", arg);
		else if (genrc_signal_reload == 0)
			genrc_no_reload = 1;
		break;

	case OPT_SIGNAL_STOP:
		genrc_signal_stop = str_to_sig(arg);
		if (genrc_signal_stop <= 0)
			usage_error("%s: invalid signal number", arg);
		break;

	case OPT_USER:
		genrc_user = xstrdup(arg);
		break;

	case OPT_VERBOSE:
		genrc_verbose++;
		break;

	default:
		exit(1);
	}
}

int
main(int argc, char **argv)
{
	char *p;
	GENRC_COMMAND command;

	mf_proctitle_init (argc, argv, environ);

	parseopt_getopt(&po, argc, argv);
	parseopt_getenv(&po);

	if (genrc_no_reload && genrc_signal_reload)
		usage_error("conflicting options: --no-reload and --signal-reload");

	if ((p = genrc_timeout) != NULL) {
		char *end;
		unsigned long n;

		if (isdigit(*p)) {
			errno = 0;
			n = strtoul(p, &end, 10);
			if (errno || *end || n > UINT_MAX)
				usage_error("%s: invalid timeout", p);
			if (n == 0)
				genrc_no_reload = 1;
			else
				genrc_start_timeout = genrc_stop_timeout = n;
		} else {
			while (*p) {
				unsigned *t;
				if (strncmp(p, "start=", 6) == 0) {
					t = &genrc_start_timeout;
					p += 6;
				} else if (strncmp(p, "stop=", 5) == 0) {
					t = &genrc_stop_timeout;
					p += 5;
				} else
					usage_error("%s: invalid timeout", p);

				errno = 0;
				n = strtoul(p, &end, 10);
				if (errno || n > UINT_MAX)
					usage_error("%s: invalid timeout", p);
				if (n > 0)
					*t = n;
				p = end;
				if (*p == ',')
					p++;
				else if (*p != 0)
					usage_error("garbage after timeout (near %s)", p);
			}
		}
	}

	if (!genrc_command) {
		if (genrc_program) {
			genrc_command = xstrdup(genrc_program);
		} else {
			usage_error("either --command (GENRC_COMMAND) or --program (GENRC_PROGRAM) must be given");
		}
	} else if (!genrc_program) {
		struct wordsplit ws;
		ws.ws_error = genrc_error;
		if (wordsplit(genrc_command, &ws,
			      WRDSF_NOCMD|WRDSF_NOVAR|
			      WRDSF_ENOMEMABRT|WRDSF_SHOWERR|WRDSF_ERROR))
			exit(1);
		genrc_program = xstrdup(ws.ws_wordv[0]);
		wordsplit_free(&ws);
	}

	if (genrc_pid_closure) {
		/* nothing */
	} else if (genrc_create_pidfile) {
		genrc_pid_closure = get_pid_closure_file(genrc_create_pidfile);
	} else {
		genrc_pid_closure = get_pid_closure(DEFAULT_PID_SOURCE);
	}

	if (genrc_log_tag == NULL)
		genrc_log_tag = genrc_program;

	if (genrc_log_file != NULL) {
		if (genrc_log_err_file != NULL)
			usage_error("both --log-file and --log-err-file are set");
		if (genrc_log_out_file != NULL)
			usage_error("both --log-file and --log-out-file are set");
	}

	parseopt_argv(&po, &argc, &argv);

	if (argc == 0)
		usage_error("missing command verb");
	if (argc > 1)
		usage_error("too many arguments");

	command = find_command(argv[0]);
	if (!command)
		usage_error("unrecognized command: %s", argv[0]);
	exit(command());
}
