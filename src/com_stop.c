/* genrc - generic system initialization script helper
   Copyright (C) 2018-2025 Sergey Poznyakoff

   Genrc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Genrc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with genrc. If not, see <http://www.gnu.org/licenses/>. */

#include "genrc.h"

static void
timermul(struct timeval *a, int b)
{
	a->tv_sec *= b;
	a->tv_usec *= b;
	a->tv_sec = a->tv_sec + a->tv_usec / 1000000;
	a->tv_usec %= 1000000;
}

#define MIN_POLL_TTW 20000

static int
pidlist_wait(PIDLIST *pids)
{
	struct timeval stoptime, before, after, ttw, maxttw;
	int ratio;

	gettimeofday(&stoptime, NULL);
	stoptime.tv_sec += genrc_stop_timeout;
	ratio = 1;

	/* Wait for the process to terminate */
	while (pids->pidc) {
		gettimeofday(&before, NULL);
		if (timercmp(&before, &stoptime, >))
			break;
		if (get_pid_list(genrc_pid_closure, pids))
			break;
		if (pids->pidc == 0)
			return 0;
		gettimeofday(&after, NULL);
		if (timercmp(&after, &stoptime, >=))
			break;
		if (ratio < 10)
			++ratio;
		timersub(&stoptime, &after, &maxttw);
		timersub(&after, &before, &ttw);
		timermul(&ttw, ratio);
		if (ttw.tv_sec < 0 || ttw.tv_usec < 0)
			ttw.tv_sec = ttw.tv_usec = 0;
		if (timercmp(&ttw, &maxttw, >))
			ttw = maxttw;
		if (ttw.tv_sec == 0 && ttw.tv_usec < MIN_POLL_TTW)
			ttw.tv_usec = MIN_POLL_TTW;
		if (select(0, NULL, NULL, NULL, &ttw) < 0 && errno != EINTR) {
			system_error(errno, "select");
			break;
		}
	}
	return pids->pidc > 0;
}

int
com_stop(void)
{
	PIDLIST pids;

	pidlist_init(&pids);
	if (get_pid_list(genrc_pid_closure, &pids)) {
		genrc_error("program status unknown");
		return 1;
	}

	if (pids.pidc == 0) {
		genrc_error("%s not running", genrc_program);
		return 1;
	}

	if (genrc_verbose > 1)
		printf("Stopping %s\n", genrc_program);

	/* Send the stop signal */
	if (genrc_kill_mode == genrc_kill_group && pids.pidc == 1)
		pids.pidv[0] = - pids.pidv[0];
	pidlist_kill(&pids, genrc_signal_stop);

	/* Wait for the process to terminate */
	if (pidlist_wait(&pids)) {
		/* Forcefully terminate the remaining processes */
		if (genrc_kill_mode == genrc_kill_mixed && pids.pidc == 1)
			pids.pidv[0] = - pids.pidv[0];
		pidlist_kill(&pids, SIGKILL);
		return pidlist_wait(&pids);
	}
	return 0;
}
