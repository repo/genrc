.\" This file is part of genrc.
.\" Copyright (C) 2018-2025 Sergey Poznyakoff.
.\"
.\" Genrc is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Genrc is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with genrc.  If not, see <http://www.gnu.org/licenses/>.
.TH GENRC 8 "March 11, 2025" "GENRC" "Genrc User Manual"
.SH NAME
genrc \- generic system initialization script helper
.SH SYNOPSIS
.nh
.na
\fBgenrc\fR\
 [\fB\-heqv\fR]\
 [\fB\-C\fR \fIDIR\fR]\
 [\fB\-F\fR \fIPIDFILE\fR]\
 [\fB\-P\fR \fISOURCE\fR]\
 [\fB\-c\fR \fICOMMAND\fR]\
 [\fB\-g\fR \fIGROUP\fR[,\fIGROUP\fR...]]\
 [\fB\-l\fR \fILIMIT\fR]\
 [\fB\-k\fR \fIMODE\fR]\
 [\fB\-p\fR \fIPROGRAM\fR]\
 [\fB\-s\fR \fISHELL\fR]\
 [\fB\-t\fR \fISECONDS\fR]\
 [\fB\-u\fR \fIUSER\fR]\
 [\fB\-\-command=\fICOMMAND\fR]\
 [\fB\-\-create\-pidfile=\fIPIDFILE\fR]\
 [\fB\-\-directory=\fIDIR\fR]\
 [\fB\-\-exec\fR]\
 [\fB\-\-group=\fIGROUP\fR[,\fIGROUP\fR...]]\
 [\fB\-\-help\fR]\
 [\fB\-\-kill\-mode=\fIMODE\fR]\
 [\fB\-\-no\-env\fR]\
 [\fB\-\-no\-reload\fR]\
 [\fB\-\-log\-err\-file=\fIFILE\fR]\
 [\fB\-\-log\-facility=\fIFACILITY\fR]\
 [\fB\-\-log\-file=\fIFILE\fR]\
 [\fB\-\-log\-out\-file=\fIFILE\fR]\
 [\fB\-\-log\-tag=\fITAG\fR]\
 [\fB\-\-limit=\fILIMIT\fR]\
 [\fB\-\-no\-reload\fR]\
 [\fB\-\-pid\-from=\fISOURCE\fR]\
 [\fB\-\-pidfile=\fIPIDFILE\fR]\
 [\fB\-\-program=\fIPROGRAM\fR]\
 [\fB\-\-quiet\fR]\
 [\fB\-\-restart\-on\-exit=\fR[\fB!\fR]\fISTATUS\fR[\fB,\fISTATUS\fR...]]\
 [\fB\-\-restart\-on\-signal=\fR[\fB!\fR]\fISIG\fR[\fB,\fISIG\fR...]]\
 [\fB\-\-sentinel\fR]\
 [\fB\-\-shell=\fISHELL\fR]\
 [\fB\-\-signal\-reload=\fISIG\fR]\
 [\fB\-\-signal\-stop=\fISIG\fR]\
 [\fB\-\-timeout=\fISECONDS\fR]\
 [\fB\-\-user=\fIUSER\fR]\
 [\fB\-\-usage\fR]\
 [\fB\-\-verbose\fR]\
 {\
 \fBstart\fR\
 |\
 \fBstop\fR\
 |\
 \fBrestart\fR\
 |\
 \fBreload\fR\
 |\
 \fBstatus\fR\
 |\
 \fBloghup\fR\
 |\
 \fBwait\fR\
 }
.ad
.hy
.SH DESCRIPTION
.B genrc
is a generic helper program for writing system initialization
scripts. Depending on the operation mode, it starts, stops,
reconfigures or displays current status of a specific program.
.PP
The operation mode of the program is set by its only mandatory
argument. Other program settings are specified via the command
line options or environment variables. Most options have a
corresponding environment variable. For example, the command line
of the program to be run is given by the \fB\-\-command\fR option,
or by the \fBGENRC_COMMAND\fR environment variable. If both the
variable is set and the option is supplied, the later takes precedence
over the former.
.PP
The \fB\-\-program\fR option or the \fBGENRC_PROGRAM\fR environment
variable supplies the name of the program, which is used to determine
whether the program is already running. If not supplied, the first
word (in the shell sense) from \fICOMMAND\fR is used.
.PP
If \fB\-\-program\fR is given, but \fB\-\-command\fR is not, its value
will be used as the command to run.
.PP
The program operation modes are:
.SS start
If given this argument, \fBgenrc\fR runs the supplied
command. Before, it checks if the program is not already running and
refuses to start its second copy if so.
.PP
It is supposed that the program to be run will detach from the
controlling terminal and continue running in the background (i.e. it
is a \fIdaemon\fR, in UNIX sense). If it is not the case, use the
\fB\-\-sentinel\fR option. With this option, \fBgenrc\fR will start
the command and become a daemon, controlling the execution
of the program. It will exit when the command terminates. So long as
the command runs, \fBgenrc\fR will pipe its standard output and error
to syslog facility \fBdaemon\fR. The standard output will be logged
with the priority \fBinfo\fR and the error with the priority
\fBerr\fR.  Alternatively, standard streams can be redirected to a
file using the \fB\-\-log\-file\fR, or to different files, using
\fB\-\-log\-err\-file\fR and \fB\-\-log\-out\-file\fR
options.
.PP
If the \fB\-\-create\-pidfile=\fIFILENAME\fR option is given together with
\fB\-\-sentinel\fR, the PID of the started command will be stored
in \fIFILE\fR. The file will be unlinked after the subsidiary command
terminates. Unless the \fB\-\-pid\-from\fR option is also given,
\fB\-\-pid\-from=FILE:\fIFILENAME\fR will be assumed.
.PP
In sentinel mode, it is possible to restart the program if it
terminates with a specific exit code or on a specific signal. This is
controlled by the \fB\-\-restart\-on\-exit\fR and
\fB\-\-restart\-on\-signal\fR options. Use this feature to ensure the
service provided by the program won't get terminated because of
hitting a bug or encountering an unforeseen external condition. For
example, the following two options make sure that the program will
be terminated only if it exits with status 0 or is delivered the
SIGTERM or SIGQUIT signal:
.EX
--restart-on-exit='!0' --restart-on-signal='!TERM,QUIT'
.EE
.PP
If restarts are requested, \fBgenrc\fR will control how often it has
to restart the program using the same algorithm as
.B init (8).
Namely, if the program is restarted more than 10 times within two
minutes, \fBgenrc\fR will disable subsequent restarts for the next
5 minutes. If the \fB\-\-create\-pidfile\fR option was used, the
PID of the controlling \fBgenrc\fR process will be stored in the
file during that interval. If the \fBSIGHUP\fR signal is delivered
during the sleep interval, the sleep will be broken prematurely and
the program restarted again.
.SS status
In \fBstatus\fR mode \fBgenrc\fR verifies if the \fICOMMAND\fR is
already running and outputs its status on the standard output. To this
effect, it uses an abstraction called \fIPID source\fR, which allows
it to determine the PID of the program.
.PP
The default PID source is the Linux \fB/proc\fR filesystem (or, if it
is not available, the output of \fBps -efw\fR), which is scanned for
the name of the program (as given by \fB\-\-program\fR or
\fB\-\-command\fR options).
.PP
The source to use can be supplied with the \fB\-\-pid\-from\fR option
(or the \fB\-\-pidfile\R option, which is equivalent to
\fB\-\-pid\-from=FILE:\fR). See the section \fBPID SOURCES\fR for a
detailed discussion of available sources.
.SS stop
In the \fBstop\fR mode \fBgenrc\fR stops the command by sending it
\fBSIGTERM\fR (or another signal, as supplied with the
\fB\-\-signal\-stop\fR option). If the PID source returns multiple
PIDs, by default only parent PID is selected. However, \fBgenrc\fR can
be instructed to signal all PIDs instead (see the \fBa\fR flag in the
description of \fBPROC\fR or \fBPS\fR PID source).
.PP
After sending the signal, the program will wait for all processes to
terminate. If they don't terminate within 5 seconds, \fBgenrc\fR will
send the \fBSIGKILL\fR signal and wait another 5 seconds for them to
terminate. The timeout can be changed using the \fB\-\-timeout\fR
option.
.PP
The way to send the signals to the program is determined by the
\fB\-k\fR (\fB\-\-kill\-mode\fR) option. If its value is \fBgroup\fR,
both \fBSIGTERM\fR and, if necessary subsequent \fBSIGKILL\fR will be
sent to all processes in the process control group.  This is the
default mode if the command is a daemon (i.e., if \fB\-\-sentinel\fR)
is not given). If the value is \fBprocess\fR, the signal will be sent
to the process individually. Finally, if the value is \fBmixed\fR, the
\fBSIGTERM\fR is sent to the main process, while the subsequent
\fBSIGKILL\fR is sent to the control group of the process.
.SS restart
Restarts the program. It is equivalent to running
.B genrc stop
immediately followed by
.BR "genrc start" .
.SS reload
Attempts to reload (or reconfigure) the program by sending it the
\fBSIGHUP\fR signal (or another signal, as given with the
\fB\-\-signal\-reload\fR option). The \fB\-\-no\-reload\fR or
\fB\-\-signal\-reload=0\fR option disables this behavior, making
this mode equivalent to
.BR "genrc restart" .
.SS loghup
Locates the parent \fBgenrc\fR process that runs the command and sends
it the signal instructing it to reopen log files created with
\fB\-\-log\-file\fR, \fB\-\-log\-err\-file\fR, and
\fB\-\-log\-out\-file\fR options.  This is intended to assist in
rotating these files.
.PP
Notice, that this mode makes sense only if the command was started
using the \fB\-\-sentinel\fR option together with at least one of the
above mentioned options.
.SS wait
Waits for the program to terminate.  Wait time is specified using the
\fB\-\-timeout\fR option and is 5 seconds by default.  Exit code is 0
if the program terminated, and 1 on time out.
.PP
Use this command verb to wait for the program if it was terminated by
some external means.
.SH EXAMPLE
Following is a sample \fBrc.ntpd\fR file for Slackware:
.sp
.EX
#! /bin/sh
PIDFILE=/var/run/ntpd.pid
exec /sbin/genrc \\
     \-\-command="/usr/sbin/ntpd \-g \-p $PIDFILE" \\
     \-\-no\-reload \\
     \-\-signal\-stop=SIGHUP \\
     \-\-pid\-from="FILE:$PIDFILE" "$@"
.EE
.SH OPTIONS
.TP
\fB\-c\fR, \fB\-\-command=\fICOMMAND\fR
Command line to run.
.TP
\fB\-\-create\-pidfile=\fINAME\fR
When used together with \fB\-\-sentinel\fR, the PID of the command
being run will be stored in file \fINAME\fR. The
\fB\-\-pid\-from=FILE:\fINAME\fR will be assumed, unless the
\fB\-\-pid\-from\fR is given explicitly (or the \fBGENRC_PID_FROM\fR
variable is set).
.TP
\fB\-C\fR, \fB\-\-directory=\fIDIR\fR
Change to directory \fIDIR\fR.
.TP
\fB\-e\fR, \fB\-\-exec\fR
In sentinel mode, run the command directly via exec(3), instead of
using \fBsh -c\fR.  Alias: \fB\-\-shell=none\fR.
.TP
\fB\-F\fR, \fB\-\-pidfile=\fINAME\fR
Name of the PID file (same as \fB\-\-pid\-from=FILE:\fINAME\fR)
.TP
\fB\-h\fR, \fB\-\-help\fR
Display a short help list.
.TP
\fB\-g\fR, \fB\-\-group=\fIGROUP\fR[,\fIGROUP\fR...]
Run program with this \fIGROUP\fR privileges. If the argument is a
list of groups, the first group becomes the principal one, and the
rest of them supplementary groups. Each \fIGROUP\fR is either a group
name or a numeric group number prefixed with a plus sign. Whatever
notation is used, the groups must exist in the system group database.

See also the \fB\-\-user\fR option.
.TP
\fB\-k\fR, \fB\-\-kill\-mode=\fIMODE\fR
Specifies how to send termination signal to the main
process. \fIMODE\fR is one of \fBgroup\fR (send termination signal to
the process control group), \fBprocess\fR (send it to the process
itself), or \fBmixed\fR (send termination signal to the process, and
send subsequent \fBSIGKILL\fR to the program control group). Refer to
the description of the \fBstop\fR command above for a detailed
discussion of these three modes.
.TP
\fB\-l\fR, \fB\-\-limit=\fILIM\fR
Set resource limit.  \fILIM\fR is a resource letter followed by
a value.  Resource letters are:
.RS
.TP
.B c
Core file size (KB).
.TP
.B d
Data size (KB).
.TP
.B f
File size (KB).
.TP
.B l
Locked-in-memory address space (KB).
.TP
.B m
Resident set size (KB).
.TP
.B n
Number of open files.
.TP
.B p
Process priority (nice value), -20..19.
.TP
.B s
Stack size (KB).
.TP
.B t
CPU time (seconds).
.TP
.B u
Number of subprocesses.
.TP
.B v
Virtual memory size (KB).
.RE
.IP
Their upper-case equivalents enforce limit setting, i.e. the command will be
started only if setting the corresponding limit succeeded.
.IP
Several limits can be set in one option by concatenating them
(with optional whitespace in between).
.TP
\fB\-\-log\-err\-file=\fIFILE\fR
Redirect the command's stderr to \fIFILE\fR.
.TP
\fB\-\-log\-facility=\fIFACILITY\fR
Selects \fBsyslog\fR facility for use in sentinel mode.  Valid arguments are
.BR auth ,
.BR authpriv ,
.BR cron ,
.BR daemon ,
.BR ftp ,
.BR lpr ,
.BR mail ,
.BR user ,
and
.BR local0
through
.BR local7 .
The default is
.BR daemon .
.TP
\fB\-\-log\-file=\fIFILE\fR
Redirect the command's standard streams to \fIFILE\fR.  This option
cannot be used together with \fB\-\-log\-err\-file\fR and \fB\-\-log\-out\-file\fR.
.TP
\fB\-\-log\-out\-file=\fIFILE\fR
Redirect the command's stdout to \fIFILE\fR.
.TP
\fB\-\-log\-tag=\fITAG\fR
Sets \fBsyslog\fR tag for use in sentinel mode.
.TP
.B \-\-no\-env
Ignore environment variables.  See
.BR ENVIRONMENT ,
for details.
.TP
\fB\-\-no\-reload\fR
Makes \fBreload\fR equivalent to \fBrestart\fR.
.TP
\fB\-p\fR, \fB\-\-program=\fIPROGRAM\fR
Name of the program to run.
.TP
\fB\-P\fR, \fB\-\-pid\-from=\fISOURCE\fR
Where to look for PIDs of the running programs.
.TP
\fB\-q\fR, \fB\-\-quiet\fR
Suppress normal diagnostic messages.  This is a counterpart of
\fB\-\-verbose\fR (see below).  Especially useful with \fBstatus\fR,
when only exit status is needed.
.TP
\fB\-\-restart\-on\-exit=\fR[\fB!\fR]\fISTATUS\fR[\fB,\fISTATUS\fR...]
This option takes effect when used together with
\fB\-\-sentinel\fR. If the program terminates with one of status
codes listed as the argument to this option, it will be immediately
restarted. The exclamation mark at the start of the list inverts the
set, e.g. \fB\-\-restart\-on\-exit='!0,1'\fR means restart unless the
program exit code is 0 or 1. Note the use of quotation to prevent the
\fB!\fR from being interpreted by the shell.
.TP
\fB\-\-restart\-on\-signal=\fR[\fB!\fR]\fISIG\fR[\fB,\fISIG\fR...]
This option takes effect when used together with
\fB\-\-sentinel\fR. If the program terminates due to receiving one of
the signals from this list, it will be immediately restarted. Each
\fISIG\fR is either a signal number, or a signal name, as listed in
.BR signal (7).
The \fBSIG\fR prefix can be omitted from the signal name. Names are
case-insensitive. Thus, \fB1\fR, \fBHUP\fR, \fBSIGHUP\fR, and
\fBsighup\fR all stand for the same signal.
.sp
The exclamation mark at the start of the list complements the signal
set, so that e.g. \fB\-\-restart\-on\-signal='!TERM,QUIT,INT'\fR will
restart the program unless it terminates on one of the listed signals.
.TP
\fB\-\-sentinel\fR
\fIPROGRAM\fR runs in foreground; disconnect from the controlling
terminal, start it and run in background until it terminates. The
program's stdout and stderr are sent to the syslog facility
\fBdaemon\fR, priorities \fBinfo\fR and \fBerr\fR, correspondingly.

The default facility and syslog tag can be changed using the
\fB\-\-log\-facility\fR and \fB\-\-log-tag\fR options.

See the options \fB\-\-restart\-on\-exit\fR and
\fB\-\-restart\-on\-signal\fR for details on how to restart the
program.
.TP
\fB\-s\fR, \fB\-\-shell=\fISHELL\fR
In sentinel mode, use \fISHELL\fR to run the command, instead of the
default
.BR /bin/sh .
\fISHELL\fR must support the \fB\-c\fR option.
Use \fB\-\-shell=none\fR or \fB\-\-exec\fR to run the command directly.
.TP
\fB\-\-signal\-reload=\fISIG\fR
Signal to send on reload (default: \fBSIGHUP\fR). Setting it to 0 is
equivalent to \fB\-\-no\-reload\fR.
.TP
\fB\-\-signal\-stop=\fISIG\fR
Signal to send in order to terminate the program (default:
\fBSIGTERM\fR).
.TP
\fB\-t\fR, \fB\-\-timeout=\fISECONDS\fR
Time to wait for the program to start up or terminate.
.IP
To set the two timeouts separately, specify \fISECONDS\fR as:
.RS
.EX
.BI "start=" N ",stop=" M
.EE
.RE
.IP
The \fBstart\fR and \fBstop\fR parameters can be given in any order,
and any of them can be omitted.
.TP
\fB\-\-usage\fR
Display a short usage summary.
.TP
\fB\-u\fR, \fB\-\-user=\fIUSER\fR
Run with this user privileges. The argument is either a login
name or a numeric UID prefixed with the plus sign. Whatever form is
used, it must correspond to a valid user from the system user
database.

Unless \fB\-\-group\fR option is also given, the primary and
supplementary groups of \fIUSER\fR will be used.
.TP
\fB\-\-version\fR
Display program version and exit.
.TP
\fB\-v\fR, \fB\-\-verbose\fR
Print verbose messages (e.g. "Starting \fIPROGNAME\fR").  Additional
\fB\-v\fR options increase verbosity level.  So far, two levels are
implemented. \fB\-vv\fR giving debugging messages.  See also \fB\-q\fR
(\fB\-\-quiet\fR), above.
.SH PID SOURCES
.TP
\fBFILE:\fIFILENAME\fR
Read PID from the file \fIFILENAME\fR.
.TP
\fBCONFIG:\fILANG\fB:\fIFILENAME\fB:\fIFQRN\fR
Name of the PID file is stored in relation \fIFQRN\fR of the configuration
file \fIFILENAME\fR, written in language \fILANG\fR. Recognizable
\fILANG\fR values are:
.RS
.TP
.B BIND
ISC BIND configuration file.
.TP
.B DHCPD
ISC DHCPD configuration file.
.TP
.B GIT
Git-style configuration file.
.TP
.B GRECS
GRECS-style configuration file. This is a generalization of a
structured configuration file format.
.TP
.B META1
META1 configuration file.
.TP
.B PATH
Configuration specified as fully-qualified keyword-value pairs
(similar to \fB.Xdefaults\fR).
.RE
.TP
\fBGREP:\fIFILE\fB:s/\fIRX\fB/\fIREPL\fB/[\fIFLAGS\fR][\fB;\fR...]
Grep for the first line in \fIFILE\fR that matches \fIRX\fR. If found,
modify the matched portion according to \fIREPL\fR and \fIFLAGS\fR. Use
the resulting string as PID. More sed expressions can be supplied,
separated with semicolons.
.TP
\fBPROC\fR[\fB:\fR[\fIEXE\fR][\fB:\fIFLAGS\fR]]
Look for matching program in \fB/proc/\fIPID\fB/*\fR. If \fIEXE\fR is
not supplied or empty, the program name from \fB\-\-program\fR will be
used. \fIFLAGS\fR are:
.RS
.TP
.B e
exact match
.TP
.B g
glob pattern match
.TP
.B x
extended POSIX regexp match (default)
.TP
.B p
PCRE match
.TP
.B i
case-insensitive match
.TP
.B c
match entire command line
.TP
.B r
match real executable name (instead of argv0)
.TP
.B a
signal all matching PIDs
.RE
.TP
\fBPS\fR[\fB:\fR[\fIEXE\fR][:\fIFLAGS\fR]]
Look for matching program in the output of \fBps \-ef\fR. \fIEXE\fR
and \fIFLAGS\fR are as described above.
.SH ENVIRONMENT
Several
.B genrc
options have environment variable equivalents.  If the option is
not supplied,
.B genrc
consults the corresponding environment variable: if it is set, its
value is taken as the option value.
.PP
If the
.B \-\-no\-env
command line option is given, this processing is disabled and the
environment is ignored.
.PP
The following table summarizes such environment variables and
corresponding options:
.sp
.nf
.ta 5n 35n
.ul
	Variable	Option
	\fBGENRC_COMMAND=\fICOMMAND\fR	\fB\-\-command=\fICOMMAND\fR
	\fBGENRC_PROGRAM=\fINAME\fR	\fB\-\-program=\fINAME\fR
	\fBGENRC_PID_FROM=\fISOURCE\fR	\fB\-\-pid\-from=\fISOURCE\fR
	\fBGENRC_TIMEOUT=\fISECONDS\fR	\fB\-\-timeout=\fISECONDS\fR
	\fBGENRC_SENTINEL=1\fR	\fB\-\-sentinel\fR
	\fBGENRC_CREATE_PIDFILE=\fINAME\fR	\fB\-\-create\-pidfile=\fINAME\fR
	\fBGENRC_USER=\fINAME\fR	\fB\-\-user=\fINAME\fR
	\fBGENRC_GROUP=\fIGROUPS\fR	\fB\-\-group=\fIGROUPS\fR
	\fBGENRC_KILL_MODE=\fIMODE\fR	\fB\-\-kill\-mode=\fIMODE\fR
	\fBGENRC_LOG_FACILITY=\fIF\fR	\fB\-\-log\-facility=\fIF\fR
	\fBGENRC_LOG_FILE=\fIFILE\fR	\fB\-\-log\-file=\fIFILE\fR
	\fBGENRC_LOG_TAG=\fISTR\fR	\fB\-\-log\-tag=\fISTR\fR
	\fBGENRC_LOG_ERR_FILE=\fIFILE\fR	\fB\-\-log\-err\-file=\fIFILE\fR
	\fBGENRC_LOG_OUT_FILE=\fIFILE\fR	\fB\-\-log\-out\-file=\fIFILE\fR
.fi
.SH EXIT STATUS
Unless given the \fBstatus\fR command verb, \fBgenrc\fR exits with
status 0 on success, and 1 if any errors occurred.  With \fBstatus\fR,
exit code 0 indicates that the command is running and 1 means it is
not.  If unable to determine command status, exit code 2 is returned.
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2018 -- 2025 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
