\input texinfo  @c -*- texinfo -*-
@setfilename genrc.info
@c %**start of header
@settitle genrc

@defcodeindex op
@defcodeindex kw

@syncodeindex fn cp
@syncodeindex vr cp
@syncodeindex op cp
@syncodeindex kw cp

@include version.texi

@ifinfo
@dircategory System Administration
@direntry
* genrc: (genrc).       Generic system initialization script helper.
@end direntry
@end ifinfo
@c %**end of header

@iftex
@finalout
@end iftex

@copying
Copyright @copyright{} 2024--2025 Sergey Poznyakoff

@quotation
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover, and no Back-Cover texts.
@end quotation
@end copying

@titlepage
@title genrc
@subtitle version @value{VERSION}, @value{UPDATED}
@author Sergey Poznyakoff.
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@ifnottex
@node Top
@top Genrc

This edition of the @cite{Genrc Manual}, last updated
@value{UPDATED}, documents @command{genrc} version @value{VERSION}.

@insertcopying
@end ifnottex
@contents

@node Overview
@chapter Overview
@cindex SysV initialization scripts
 This program emerged as a result of several decades of writing
SysV-style initialization scripts and inspecting such scripts written
by others.

 SysV-style scripts are shell scripts executed during system startup and
shutdown.  They are normally located in @file{/etc/init.d} directory
and symlinked to several @file{/etc/rc@var{n}.d} directories (where
@var{n} is a decimal @dfn{runlevel number}), where they are looked
up by the @command{init} process.  On other systems
(e.g. @command{Slackware}), they are located in @file{/etc/rc.d}
directory.

 Each such script accepts a single argument -- a @dfn{command verb} --
and performs the requested action depending on it.  For example,
@code{start} instructs the script to start up a service it was written
for, @code{stop} instructs it to shut it down, and so on.  All command
verbs form a fixed set.

@cindex service
 The @command{genrc} tool is a generic helper program for writing
SysV-style scripts.  Depending on the operation mode, it starts, stops,
reconfigures or displays current status of a specific program
(usually, a daemon), hereinafter referred to as @dfn{service}.

@cindex service command line
@opindex --command, introduced
@kwindex GENRC_COMMAND, introduced
 The operation mode of the program is requested by the command verb,
given as its only mandatory argument.  Other program settings are
specified via the command line options or environment variables.
Most options have a corresponding environment variable.  For example,
the command line of the service to start is given either by the
@option{--command} option, or by the @env{GENRC_COMMAND} environment
variable.  If the variable is set and the option is supplied at the
same time, the later takes precedence over the former.

@cindex service program
@cindex service executable
@opindex --program, introduced
@kwindex GENRC_PROGRAM, introduced
 The @option{--program} option (or the @env{GENRC_PROGRAM} environment
variable) supplies the @dfn{service program name}, i.e. the name of
the executable file to be run or monitored.  If not given explicitly,
the first word (in the shell sense) from the command is used. 

 The service run by @command{genrc} is usually a @dfn{daemon},
i.e. a program that, upon startup, detaches itself from the
controlling terminal and continues operating in background.  However,
regular foreground programs can be used too.  For such cases,
@command{genrc} can itself work as a wrapper over the service,
running it in the background (@pxref{Sentinel}).

Calling @command{genrc} with appropriate command word allows you to
perform various operation on the service, such as inspecting
its state (running or not), stopping or restarting it, etc.

To give you the feel of what @command{genrc} can do, here is an
example of startup script for @code{ntpd}:

@example
@group
#! /bin/sh
PIDFILE=/var/run/ntpd.pid
exec /sbin/genrc \
     --command="/usr/sbin/ntpd -g -p $PIDFILE" \
     --no-reload \
     --signal-stop=SIGHUP \
     --pid-from="FILE:$PIDFILE" "$@@"
@end group
@end example

See below for the meaning of each particular option.

@node Commands
@chapter Command Verbs
@cindex command verbs
 @command{Genrc} understands the following command verbs:

@deffn {Command verb} start
Start the service.  Before startup, @command{genrc} will verify
if the service is not already running.  It will refuse to start its
second copy if so.
@end deffn

@deffn {Command verb} status
In @code{status} mode, @command{genrc} verifies if the service is
already running and outputs its status on the standard output.  To this
effect, it uses an abstraction called @dfn{PID source}, which allows
it to determine PID of the running instance of the service command
(@pxref{PID source}).

In this mode @command{genrc} exits with code 0 if the service is
running and 1 if it is not.  If unable to get service status, it exits
with code 2.

Use the @option{-q} (@option{--quiet}) option to suppress verbose
diagnostics, e.g. when using @command{genrc} in shell conditionals, e.g.:

@example
@group
if genrc -q status; then
   ...
fi
@end group
@end example
@end deffn

@deffn {Command verb} stop
@command{Genrc} locates the running instance of the service and stops
it by sending it @code{SIGTERM} (or another signal, as supplied with the
@option{--signal-stop} option).  If the PID source returns multiple
PIDs, by default only parent PID is selected.  However,
@command{genrc} can be instructed to signal all PIDs instead (see the
@samp{a} flag in @ref{PROC and PS}).

After sending the signal, @command{genrc} waits for all service
processes to terminate.  If they don't terminate within 5 seconds, it
will send the @code{SIGKILL} signal and wait another 5 seconds for
them to terminate. The timeout is controlled by the @option{--timeout}
option (@pxref{timeout}).

@cindex program termination mode
@opindex -k, introduced
@opindex --kill-mode, introduced
The way to signal the service program is determined by the @option{-k}
(@option{--kill-mode}) option.  If its value is @samp{process}, then
both @code{SIGTERM} and, if necessary, subsequent @code{SIGKILL} will
be sent to the service process itself.  If it is set to @samp{group},
then both signals will be sent to all processes in the control
group of the service.  Finally, it its value is @samp{mixed}, the
@code{SIGTERM} will be sent to the main process, and then, if it fails
to terminate within the preconfigured timeout (@pxref{timeout}), the
@code{SIGKILL} will be sent to the control group of the process.

The default is @samp{process}.
@end deffn

@deffn {Command verb} restart
Restarts the service.  It is equivalent to running @command{genrc stop}
immediately followed by @command{genrc start}.
@end deffn

@deffn {Command verb} reload
Attempts to reload (or reconfigure) the service by sending it the
@code{SIGHUP} signal (or another signal, as given with the
@option{--signal-reload} option). The @option{--no-reload} or
@option{--signal-reload=0} option disables this behavior, making
this mode equivalent to @command{genrc restart}.
@end deffn

@deffn {Command verb} loghup
@cindex log file rotation
Locates the parent @command{genrc} process that runs the command and
sends it a signal instructing it to reopen log files created with
@option{--log-file}, @option{--log-err-file}, and
@option{--log-out-file} options at startup (@pxref{logging}.  This is
intended to assist in rotating these files.

Notice, that this command verb makes sense only if the service was
started using the @option{--sentinel} option together with at least
one of the above mentioned options.  @xref{Sentinel}, for details.
@end deffn

@deffn {Command verb} wait
@cindex wait for program to finish
Wait for the service to finish.  @command{genrc wait} exits when
the service terminates or the @dfn{stop timeout} expires, whichever
happens first.  The timeout is set by @option{-t} (@option{--timeout})
option (@pxref{timeout}) and is 5 seconds by default.  @command{genrc
wait} exits with code 0, if the service terminated, and 1 otherwise.

Use this command verb to wait for the service which is supposed to be
terminated by some external means.  For example, one possible way to
implement the @code{stop} command for @code{named} is:

@example
@group
# Stop bind graciously.
rndc stop
# If it doesn't stop within allotted time, terminate it forcefully.
if ! genrc wait; then
   genrc stop
fi
@end group
@end example
@end deffn

@node PID source
@chapter PID Source
@opindex --pid-from
@opindex GENRC_PID_FROM
 In order to stop and restart the service, @command{genrc}
needs to know its PID.  Several mechanisms that are able to provide
this information are collectively known as @dfn{PID sources}.  Only
one PID source can be active at a time.  It is selected using the
@option{--pid-from} command line option, or @env{GENRC_PID_FROM}
environment variable.

 The PID source is identified by its @dfn{name} and optional
@dfn{parameters}, separated by colons.  Source name and parameters
together form a @dfn{source specification}.  Available source names are:
known: @samp{FILE}, @samp{CONFIG}, @samp{GREP}, @samp{PROC},
and @samp{PS}.  These are discussed in detail in the sections that
follow.

@node FILE
@section FILE

@deffn {PID spec} FILE:@var{filename}
 This PID source reads the PID number from the first line of a named
file.  Use it if the service started by @command{genrc} stores its
PID in a file.  E.g.:

@example
grecs --pid-from=FILE:/run/named.pid ...
@end example
@end deffn

@node GREP
@section GREP

@deffn {PID spec} GREP:@var{filename}:s/@var{rx}/@var{repl}/[@var{flags}][;...]
This is a generalization of the @samp{FILE} source.  The file @var{filename}
is searched for a line matching the regular expression @var{rx}.  If
found, the replace string @var{repl} is evaluated and the result is
used as PID.
@end deffn

@cindex sed expression
The @code{s//} part is, generally speaking, the same as
in @command{sed} command (@pxref{The "s" Command, The "s" Command, The
`s' Command, sed, GNU sed}).  The difference between @command{sed} and
@code{GREP} source is that @code{GREP} stops when the first matching
line is found.

As in @command{sed}, more than one @code{s//} expression can be given,
separated by semicolons.  In that case, the result of evaluation of
@var{repl} in an expression serves as input to the expression that
follows it.

Supported @var{flags} are:

@table @samp
@item b
Use basic regular expression.  This is the default.  See also the
@samp{x} flag, below.

@item g
Apply the replacement to @emph{all} matches to the @var{regexp}, not
just the first.

@item i
Use case-insensitive matching.

@item x
@var{regexp} is an @dfn{extended regular expression} (@pxref{Extended
regexps, Extended regular expressions, Extended regular expressions,
sed, GNU sed}).

@item @var{number}
Only replace the @var{number}th match of the @var{regexp}.

If @samp{g} and @var{number} modifiers are used together, then all
matches before @var{number}th are ignored, and the remaining ones
are processed as specified by @samp{g}.
@end table

 For example, if file @file{/run/prog.stat} contains several lines
in @samp{@var{key}=@var{value}} format and, among them, the @var{key}
@samp{PidNumber} holds the PID, it can be obtained using the following
source specification:

@example
genrc --pid-from='GREP:s/^PidNumber=(.+)/\1/x'
@end example

@node CONFIG
@section CONFIG

@deffn {PID spec} CONFIG:@var{type}:@var{filename}:@var{ref}
The @var{filename} parameter is the name of a configuration file
and @var{type} is one of the predefined configuration file types.
The file is parsed and searched for a statement that matches the
reference @var{ref}.  If found, the value of that statement is taken
as the name of a file, the first line of which contains the PID.
@end deffn

Following values are valid for the @var{type} parameter:

@table @code
@kwindex BIND
@cindex bind configuration format
@item BIND
ISC BIND configuration file
(see @uref{https://bind9.readthedocs.io/en/latest/reference.html,
configuration reference}).

@kwindex DHCPD
@cindex dhcpd configuration format
@item DHCPD
ISC DHCPD configuration file. @uref{https://www.isc.org/dhcp/}.

@kwindex GIT
@cindex git configuration format
@item GIT
Git-style configuration file.  See the section @samp{Syntax}
in git-config(1) manpage.

@kwindex GRECS
@cindex grecs configuration format
@item GRECS
GRECS-style configuration file. This is a generalization of a
structured configuration file format.  See
@uref{https://www.gnu.org.ua/software/direvent/manual/syntax.html,
its syntax reference}.

@kwindex META1
@cindex MeTA1 configuration format
@item META1
META1 configuration file.  See
@uref{http://www.meta1.org/current/doc/README.html#tth_sEc3.2, syntax
reference}.

@kwindex PATH
@cindex path configuration format
@item PATH
Configuration specified as fully-qualified keyword-value pairs
(similar to @file{.Xdefaults} file).
@end table

@cindex keyword path reference
The @var{ref} part is a @dfn{path reference} that identifies the
configuration statement to find.  It is a dot-separated list of
statement names that enclose the statement in question.  For example,
if the configuration file in BIND-like format contains:

@example
@group
global @{
  options @{
    pid-file "filename";
  @};
@};
@end group
@end example

@noindent
Then the @code{pid-file} statement is identified by the reference
@samp{.global.options.pid-file}.  More specifically, the reference
is written according to the following rules:

@enumerate 1
@item
Reference is written from top down.

@item
An untagged statement is represented by its identifier.

Examples of untagged statements are:

@example
level 1;
option @{
  ...
@}
@end example

@item
A tagged statement is represented by its identifier, immediately followed by an
equals sign, followed by the tag.

E.g. the statement:

@example
@group
options global @{
  ...
@}
@end group
@end example

@noindent
is represented by @samp{options=global}.

@item
Identifiers and values which contain whitespace, double-quotes or dots
are enclosed in double-quotes.

@item
Within double-quotes, a double-quote is represented as @samp{\"} and a
backslash is represented as @samp{\\}.

@item
Reference components are separated by dots.
@end enumerate

For example, to obtain PID of the running BIND9 @command{named}
process:

@example
genrc --pid-source=CONFIG:BIND:/etc/named.conf:.options.pid-file
@end example

@node PROC and PS
@section PROC and PS

@deffn {PID source} PROC:@var{exe}:@var{flags}
@deffnx {PID source} PROC:@var{exe}
@deffnx {PID source} PROC
@cindex proc filesystem
Look for matching program in @file{/proc} filesystem.  This method is
mostly for GNU/Linux distributions.  The executable name is supplied
by the @var{exe} parameter.  If missing or empty, the value from
@option{--program} option or the first word from the value of
@option{--command} option will be used.

Optional @var{flags} consist of one or more of the following letters:

@table @samp
@item e
@cindex exact matching
Match service program name exactly.
@item g
@cindex glob pattern matching
Use glob pattern match.
@item x
@cindex regular expression, POSIX extended
Use extended POSIX regexp match (default).  @emph{Notice}, that when
using named character classes, you'll have to escape semicolons, to
prevent them from being interpreted as specification parameter
separators.  For example, to use the regexp
@samp{sh[[:space:]]+command}, write it as:

@example
PROC:sh[[\:space\:]]+command:c
@end example
@noindent
or
@example
'PROC:"sh[[:space:]]+command":c'
@end example

@noindent
Note double quoting in the last example: the outer single quotes
serve to prevent inner double-quotes from being interpreted by the
shell.

See also the @samp{c} flag, below.
@item p
@cindex regular expression, Perl-compatible
@cindex PCRE
Use PCRE match.
@item i
@cindex case-insensitive
Case-insensitive match
@item c
@cindex match command line
Match entire command line, instead of matching executable name only.
@item r
@cindex match real executable name
Match real executable name (instead of @samp{argv[0]})
@item a
Signal all matching PIDs.
@end table

The flags @samp{e}, @samp{g}, @samp{x}, and @samp{p} are mutually
exclusive.

For example, if @command{talker} is a shell script, the following
source specification can be used to find its PID:

@example
PROC:sh.*talker:c
@end example
@end deffn

@deffn {PID source} PS:@var{exe}:@var{flags}
@deffnx {PID source} PS:@var{exe}
@deffnx {PID source} PS
Look for matching program in the output of @command{ps -ef}.  See
@code{PROC}, for a description of @var{exe} and @var{flags}.
@end deffn

@node Sentinel
@chapter Sentinel
@cindex sentinel mode
@cindex running foreground programs
@cindex foreground program
  Most often, @command{genrc} is used to maintain UNIX daemons, i.e.
programs that continue to run in background after startup.  However,
it can also be used to run any usual, foreground, process as well.  To
do so, use the @option{--sentinel} option.

@opindex --sentinel
  When given the @option{--sentinel} option, @command{genrc} will
start the service command and become a daemon, controlling its
execution.  It will exit when the command terminates.  So long as the
command runs, @option{genrc} will log anything it produces on its
standard output and error to syslog or to disk files.  By default,
syslog facility @samp{daemon} is used.  Output logging is controlled
by the following options:

@anchor{logging}
@cindex logging
@table @option
@opindex --log-facility
@item --log-facility=@var{facility}
Log diagnostic messages to this syslog facility, instead of
@samp{daemon}.  Usual facility names are accepted.

@opindex --log-tag
@item --log-tag=@var{string}
Tag syslog messages with that string.  By default, executable name is
used.

@opindex --log-file
@item --log-file=@var{file}
Redirect both standard streams to @var{file}.

@opindex --log-err-file
@item --log-err-file=@var{file}
Redirect the program standard error to @var{file}, instead of logging
it to syslog.

@opindex --log-out-file
@item --log-out-file=@var{file}
Redirect the program standard output to @var{file}, instead of logging
it to syslog.
@end table

When redirecting to a file, use either @option{--log-file}, or
a combination of @option{--log-err-file} and @option{--log-out-file}
options.  When a single stream-specific option is used
(@option{--log-err-file} or @option{--log-out-file}), the
corresponding stream will be redirected to a file, and the remaining
one will still go to syslog.

Using @option{--log-err-file=@var{file} --log-out-file=@var{file}} is
equivalent to @option{--log-file=@var{file}}.

@cindex logfile rotation
When using these options, the @command{loghup} command verb can
be used to instruct @command{genrc} to reopen the associated files.
Use it when rotating your log files.

If the @option{--create-pidfile=@var{filename}} option is given,
the PID of the started service command will be stored in FILE. The file
will be unlinked after the service terminates.  Unless the
@option{--pid-from} option is given explicitly,
@option{--pid-from=FILE:@var{filename}} will be assumed (@pxref{FILE}).

@cindex restarts in sentinel mode
In sentinel mode, it is possible to restart the service if it
terminates with a specific exit code or on a specific signal.  This is
controlled by the @option{--restart-on-exit} and @option{--restart-on-signal}
options.  Use this feature to ensure the service won't get terminated
because of hitting a bug or encountering an unforeseen external
condition.  For example, the following two options make sure that the
program will be terminated only if it exits with status 0 or is
delivered the SIGTERM or SIGQUIT signal:

@example
--restart-on-exit='!0' --restart-on-signal='!TERM,QUIT'
@end example

If restarts are requested, @command{genrc} will control how often it has to
restart the service using the same algorithm as @command{init}.  Namely, if
the service restarts more than 10 times within two minutes, @command{genrc}
will disable subsequent restarts for the next 5 minutes.  If the
@option{--create-pidfile} option was used, the PID of the controlling
@command{genrc} process will be stored in the file during that
interval.  If the @code{SIGHUP} signal is delivered during that
interval, the sleep will be broken prematurely and the program
restarted again.

@node Invocation
@chapter Invocation
 The invocation syntax is:

@example
genrc [@var{options}] @var{verb}
@end example

@xref{Commands}, for a discussion of the @var{verb} argument and
its possible values.

The options can be subdivided into the following categories:

@menu
* general::             Specify command to run.
* additional::          Additional configuration.
* privileges::          Specify runtime privileges and limits.
* sentinel::            Options valid in sentinel mode.
* informational::       Informational options.
@end menu

@node general
@section General Options
  General options supply the command to run.

@table @option
@opindex -c
@opindex --command
@item -c @var{command}
@itemx --command=@var{command}
Supplies service command line.

@opindex -p
@opindex --program
@item -p @var{program}
@itemx --program=@var{program}
Supplies service executable program name.
@end table

At least one of these options must be given.  If @var{program} is
supplied, but @var{command} is not, @var{command} is assumed to be the
same as @var{program} (i.e. @var{program} runs without command line
parameters).  Otherwise, if @var{command} is set, but @var{program} is
not, @var{program} is set to the first word (in the shell sense) in
@var{command}.

The value of @var{program} (either set explicitly, or derived as
described above) is used as a key for @code{PROC} and @code{PS} PID
sources (@pxref{PROC and PS}).

@node additional
@section Additional Options

@table @option
@opindex -k
@opindex --kill-mode
@item -k @var{mode}
@item --kill-mode=@var{mode}
Set service termination mode.  Valid values for @var{mode} are:

@table @option
@kwindex group
@item group
Send both the @code{SIGTERM} and subsequent @code{SIGKILL} (if
necessary) to the process control group of the service.

@kwindex process
@item process
Send both the @code{SIGTERM} and subsequent @code{SIGKILL} (if
necessary) to the main service process.  This is the default.

@kwindex mixed
@item mixed
Send @code{SIGTERM} to the process and subsequent @code{SIGKILL} to
the process control group.
@end table

@anchor{timeout}
@opindex -t
@opindex --timeout
@item -t @var{seconds}
@itemx --timeout=@var{seconds}
Time to wait for the service to start up or terminate.  To set the
two timeouts separately use the following form:

@example
--timeout=start=@var{n},stop=@var{m}
@end example

@noindent
The @samp{start} and @samp{stop} parameters can be given in any order.
Any of them can be omitted.

@opindex -P
@opindex --pid-from
@item -P @var{source}
@itemx --pid-from=@var{source}
Where to look for PIDs of the running service.  @xref{PID source},
for a detailed discussion.

@opindex -F
@opindex --pidfile
@item -F @var{filename}
@item --pidfile=@var{filename}
Name of the PID file (a shortcut for @option{--pid-from=FILE:@var{filename}}).

@opindex --no-env
@item --no-env

Ignore environment variables.  Use only command line options for
configuration.  @xref{Environment}, for a detailed discussion.

@opindex --no-reload
@item --no-reload
Makes @code{reload} verb equivalent to @code{restart}.  Use this if
the command does not support the notion of reloading.

@opindex -S
@opindex --sentinel
@item -S
@itemx --sentinel
The service runs in foreground; disconnect from the controlling
terminal, run it and act as a sentinel.  @xref{Sentinel}, for a
detailed description of this mode.  @xref{sentinel}, for a summary of
options available for this mode only. 

@opindex --signal-reload
@item --signal-reload=@var{sig}
Signal to send to the service program when run with @code{reload} command
verb.  The argument is either a numeric signal number, or one of the
predefined signal names (@pxref{Signals}).  Default is @code{SIGHUP}.

Using @option{--signal-reload=0} is equivalent to
@option{--no-reload}.

@opindex --signal-stop
@item --signal-stop=@var{sig}
Signal to send in order to terminate the program.  The argument is
either a numeric signal number, or one of the predefined signal names
(@pxref{Signals}).  Default is @code{SIGTERM}.

@opindex -v
@opindex --verbose
@item -v
@itemx --verbose
Enable verbose diagnostics
@end table

@node privileges
@section Runtime Privileges and Limits

@table @option
@opindex -u
@opindex --user
@item -u @var{user}
@itemx --user=@var{user}
Run service with this user privileges.  The argument is either a login
name or a numeric UID prefixed with the plus sign.  Whatever form is
used, it must correspond to a valid user from the system user
database.

Unless @option{--group} option is also given, the primary and
supplementary groups of @var{user} will be set.

@opindex -g
@opindex --group
@item -g @var{group}
@itemx --group=@var{group}[,@var{group}...]
Run service with this group privileges.  If the argument is a
list of groups, the first group becomes the principal one, and the
rest of them supplementary groups.  Each @var{group} is either a group
name or a numeric group number prefixed with a plus sign.  Whatever
notation is used, the groups must exist in the system group database.

@anchor{resources}
@opindex -l
@opindex --limit
@item -l @var{lim}
@itemx --limit=@var{lim}
@cindex resources
@cindex setting resources
Set resource limit.  @var{lim} is a resource letter followed by
a value.  Resource letters are:

@table @samp
@item c
@itemx C
@cindex core file size
Core file size (KB).
@item d
@itemx D
@cindex data size
Data size (KB).
@item f
@itemx F
@cindex file size
File size (KB).
@item l
@itemx L
@cindex locked-in-memory address space
Locked-in-memory address space (KB).
@item m
@itemx M
@cindex resident set size
Resident set size (KB).
@item n
@itemx N
@cindex number of open files
Number of open files.
@item p
@itemx P
@cindex process priority
Process priority (-20..19).
@item s
@itemx S
@cindex stack size
Stack size (KB).
@item t
@itemx T
@cindex CPU time
CPU time (seconds).
@item u
@itemx U
@cindex number of subprocesses
Number of subprocesses.
@item v
@itemx V
@cindex virtual memory size
Virtual memory (KB).
@end table

@cindex resources, enforcing
@cindex enforcing resources
Upper-case letters enforce limit setting, i.e. the command will be
started only if setting the corresponding limit succeeded.

Several limits can be set in one option by concatenating them
(with optional whitespace in between).  For example, the following
three options are equivalent:

@example
-lp-10 -lN65535 -lc0
-l 'p-10 N65535 c0'
-lp-10N65535c0
@end example
@end table

@node sentinel
@section Sentinel Mode Options

These options control the sentinel mode, a mode used to run foreground
programs.  @xref{Sentinel}, for a detailed discussion.

@table @option
@opindex --create-pidfile
@item --create-pidfile=@var{filename}
Store PID of the service program in file @var{filename}.  This implies
@option{--pid-from=FILE:@var{filename}}, unless @option{--pid-from}
option is given explicitly.

@opindex -e
@opindex --exec
@item -e
@itemx --exec
Same as @option{--shell=none}, i.e. execute the program directly
instead of via @code{sh -c}.

@opindex --log-facility
@item --log-facility=@var{facility}
Log diagnostic messages to this syslog facility, instead of
@samp{daemon}.  Valid facility names are: @samp{auth},
@samp{authpriv}, @samp{cron}, @samp{daemon}, @samp{ftp}, @samp{lpr},
@samp{mail}, @samp{user}, and @samp{local0} through @samp{local7}.

@opindex --log-tag
@item --log-tag=@var{string}
Tag syslog messages with that string.  By default, executable name is
used.

@opindex --log-err-file
@item --log-err-file=@var{file}
Redirect the program standard error to @var{file}, instead of logging
it to syslog.

@opindex --log-out-file
@item --log-out-file=@var{file}
Redirect the program standard output to @var{file}, instead of logging
it to syslog.

@opindex -s
@opindex --shell
@item -s @var{shell}
@item --shell=@var{shell}
Use @var{shell} instead of @command{/bin/sh} to run the command.
The shell must accept the @option{-c} option.

Use @option{--shell=none} to exec program directly.

@opindex --restart-on-exit
@item --restart-on-exit=[!]@var{status}[,...]
Restart service if it exits with one of the listed status codes.
Exclamation sign negates the set: @option{--restart-on-exit=!2,5}
means any exit code @emph{except} 2 and 5.

When selecting status codes, please note the special meaning of the
following status codes:

@table @asis
@item 126
The program was not started because setting an enforced resource
limit failed (@pxref{resources}).

@item 127
The program was not found.
@end table

@opindex --restart-on-signal
@item --restart-on-signal=[!]@var{sig}[,...]
Restart service if it terminates on one of the listed signals.
Arguments are either numeric signal numbers, or predefined signal
names (@pxref{Signals}).  The @samp{!} sign negates the set.
@end table

@node informational
@section Informational Options

These options cause @command{genrc} to display additional information
and exit.  When any informational option is used, no other options are
consulted, and command verb is not used.

@table @option
@opindex -h
@opindex --help
@item -h
@itemx --help
Display a short help summary.

@opindex --usage
@item --usage
Display short usage information.

@opindex --version
@item --version
Display program version and copyright information.
@end table

@node Environment
@chapter Environment Variables
@cindex environment
  Some most often used options have a corresponding environment
variable.  These are provided to make start scripts using
@command{genrc} more readable.  For any given option, if both the
environment variable is set and the option is supplied at the
same time, the later takes precedence over the former.

@opindex --no-env
This feature is disabled by the @option{--no-env} option: if given,
only command line options are considered and the environment is ignored.

@cindex export environment variables
  Don't forget to export these environment variables before invoking
@command{genrc}.

The list of environment variables and corresponding command line
options:

@multitable @columnfractions 0.50 .5
@headitem Variable @tab Option
@kwindex GENRC_COMMAND
@item @env{GENRC_COMMAND=@var{command}} @tab @option{--command=@var{command}}
@kwindex GENRC_PROGRAM
@item @env{GENRC_PROGRAM=@var{name}} @tab @option{--program=@var{name}}
@kwindex GENRC_PID_FROM
@item @env{GENRC_PID_FROM=@var{source}} @tab @option{--pid-from=@var{source}}
@kwindex GENRC_TIMEOUT
@item @env{GENRC_TIMEOUT=@var{seconds}} @tab @option{--timeout=@var{seconds}}
@kwindex GENRC_SENTINEL
@item @env{GENRC_SENTINEL=1} @tab @option{--sentinel}
@kwindex GENRC_CREATE_PIDFILE
@item @env{GENRC_CREATE_PIDFILE=@var{name}} @tab @option{--create-pidfile=@var{name}}
@kwindex GENRC_USER
@item @env{GENRC_USER=@var{name}} @tab @option{--user=@var{name}}
@kwindex GENRC_GROUP
@item @env{GENRC_GROUP=@var{groups}} @tab @option{--group=@var{groups}}
@kwindex GENRC_KILL_MODE
@item @env{GENRC_KILL_MODE=@var{mode}} @tab @option{--kill-mode=@var{mode}}
@kwindex GENRC_LOG_FACILITY
@item @env{GENRC_LOG_FACILITY=@var{name}} @tab @option{--log-facility=@var{name}}
@kwindex GENRC_LOG_TAG
@item @env{GENRC_LOG_TAG=@var{str}} @tab @option{--log-tag=@var{str}}
@kwindex GENRC_LOG_ERR_FILE
@item @env{GENRC_LOG_ERR_FILE=@var{file}} @tab @option{--log-err-file=@var{file}}
@kwindex GENRC_LOG_OUT_FILE
@item @env{GENRC_LOG_OUT_FILE=@var{file}} @tab @option{--log-out-file=@var{file}}
@end multitable

Here is the example script from the first chapter rewritten using
environment variables instead of options:

@example
@group
#! /bin/sh
PIDFILE=/var/run/ntpd.pid
export GENRC_COMMAND="/usr/sbin/ntpd -g -p $PIDFILE"
export GENRC_PID_FROM="FILE:$PIDFILE"
exec /sbin/genrc --no-reload --signal-stop=SIGHUP "$@@"
@end group
@end example

@noindent

@node Reporting Bugs
@chapter How to Report a Bug

Please, report bugs and suggestions to @email{gray@@gnu.org}.
You can also use a bug tracker at
@uref{https://puszcza.gnu.org.ua/projects/genrc} (requires subscription).

  You hit a bug if at least one of the conditions below is met:

@itemize @bullet
@item @command{genrc} terminates on signal 11 (SIGSEGV) or 6 (SIGABRT).
@item The program fails to do its job as described in this manual.
@end itemize

  If you think you've found a bug, please be sure to include maximum
information available to reliably reproduce or, at least, to analyze
it.  Here is the minimum set:

@itemize
@item Version of the package you are using (@command{genrc --version}).
@item Command line options used.
@item Conditions under which the bug appears.
@end itemize

  Any errors, typos or omissions found in this manual also qualify as
bugs.  Please report them, if you happen to find any.

@node Downloads
@chapter Downloads
  Stable versions of @command{genrc} are available for download from
@uref{https://download.gnu.org.ua/release/genrc, genrc distribution
server}.

Latest sources are available from the
@uref{https://git.gnu.org.ua/genrc.git/, git repository}.

  Program development page is @uref{https://puszcza.gnu.org.ua/projects/genrc}.

@node Examples
@appendix Examples
  This appendix contains several examples of @command{genrc}-based
initialization scripts, mostly taken from author's servers.  Some of
them use only command line options, some others prefer environment
variable.  In each case, that preference is solely for illustrative
purposes.

@node vsfptd
@appendixsec vsftpd
@example
@group
#!/bin/sh
exec /sbin/genrc --command=/usr/sbin/vsftpd "$@@"
@end group
@end example

Perhaps this one is the shortest of all possible scripts.  You can use
any program instead of @command{vsftp}, generally speaking.

@node proftpd
@appendixsec proftpd
This script runs @command{proftpd} and uses its configuration file to
find the location of the PID file.

@example
@group
#!/bin/sh
export GENRC_PROGRAM GENRC_PID_FROM
GENRC_PROGRAM=/usr/sbin/proftpd
GENRC_PID_FROM='GREP:/etc/proftpd.conf:"s/^[[:space:]]*PidFile[[:space:]]*//"'
exec /sbin/genrc "$@@"
@end group
@end example

@node named
@appendixsec named
This one runs ISC @command{named}.  Additional configuration is
obtained from @file{/etc/default/named}.  PID file location is read
from the configuration file.

@example
@group
#!/bin/sh
[ -f /etc/default/named ] && . /etc/default/named
export GENRC_COMMAND="/usr/sbin/named $NAMED_OPTIONS"
export GENRC_PID_FROM='BIND:/etc/named.conf:.options.pid-file'
exec /sbin/genrc "$@@"
@end group
@end example

@node local-persist
@appendixsec local-persist
The @command{local-persist} binary runs in foreground, so its startup
script uses sentinel mode (@pxref{Sentinel}).  The program will be
restarted, unless it terminates with status 0 or by @code{SIGTERM} or
@code{SIGQUIT} signals.

@example
@group
#!/bin/sh
exec /sbin/genrc \
     --command "/usr/bin/local-persist" \
     --sentinel \
     --create-pidfile=/run/local-persist.pid \
     --restart-on-exit='!0' \
     --restart-on-signal='!TERM,QUIT' "$@@"
@end group
@end example

@node Signals
@appendix Signals
  Command line options that requires signal specification as their
argument accept both the number of a signal or its symbolic name from
the table below.  The name is taken as case-insensitive and the
@samp{SIG} prefix can be omitted, i.e. the following refer to the same
signal: 1, @samp{SIGHUP}, @samp{HUP}, @samp{hup}.

@itemize @bullet
@item SIGABRT
@item SIGALRM
@item SIGBUS
@item SIGCHLD
@item SIGCONT
@item SIGFPE
@item SIGHUP
@item SIGILL
@item SIGINT
@item SIGIO
@item SIGIOT
@item SIGKILL
@item SIGPIPE
@item SIGPOLL
@item SIGPROF
@item SIGPWR
@item SIGQUIT
@item SIGSEGV
@item SIGSTKFLT
@item SIGSTOP
@item SIGSYS
@item SIGTERM
@item SIGTRAP
@item SIGTSTP
@item SIGTTIN
@item SIGTTOU
@item SIGURG
@item SIGUSR1
@item SIGUSR2
@item SIGVTALRM
@item SIGWINCH
@item SIGXCPU
@item SIGXFSZ
@end itemize

@node Index
@unnumbered Index
@printindex cp

@ifset WEBDOC
@ifhtml
@node This Manual in Other Formats
@unnumbered This Manual in Other Formats
@include otherdoc.texi
@end ifhtml
@end ifset

@bye
